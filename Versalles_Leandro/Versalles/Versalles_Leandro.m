clear;clc;close all;
load Inversores_onlyneededinputs.mat
load Analizadordeplanta_onlyneededinputs.mat
load Estacionesmeteorologicas_onlyneededinputs.mat

%Estos .mat modificados contienen solo los datos que la herramienta
%necesita, de esta manera, solo se tratarán los datos necesarios y no todos

%% Pre procesado de datos

%Los datos presentan algunos problemas de medida:
%   1) El 25 de octubre a las 1:59 se repiten datos para las mismas horas,
%   siendo los datos repetidos NaN --> eliminar
%   2) El 4 de abril los datos de las 2:00 a las 3:00 están desaparecidos
%   puesto que es de noche se replicarán los datos de 3:00 a 4:00

% 1) ---
Analizadordeplanta(77880:77939,:) = []; %los numeros se podrían cambiar por un find
Estacionesmeteorolgicas(77880:77939,:) = [];
Inversores(77880:77939,:) = [];
% 2) ---
%primero generar un vector de tiempos que represente las fechas
%desaparecidas
t1 = datetime(2021,4,4,2,0,0);
t2 = datetime(2021,4,4,2,59,0);
Datetime = t1:minutes(1):t2;

New = table(Datetime');
New.Properties.VariableNames = {'Datetime'};
%se añade las nuevas fechas con sus datos
Analizadordeplanta = [Analizadordeplanta(1:309720,:); [New Analizadordeplanta(308281:308340,2:end)]; Analizadordeplanta(309721:end,:)];
Inversores = [Inversores(1:309720,:); [New Inversores(308281:308340,2:end)]; Inversores(309721:end,:)];
Estacionesmeteorolgicas = [Estacionesmeteorolgicas(1:309720,:); [New Estacionesmeteorolgicas(308281:308340,2:end)]; Estacionesmeteorolgicas(309721:end,:)];
% ------------------------------------
% A continuación se identifican los elementos NaN presentes en los datos.
% Si una fila tiene un elemento NaN, su posición en un vector columna
% lógico es indicada con un 0 (false). Posteriormente solo se seleccionan las
% columnas con valores 1 (true)
A = ~any(ismissing(Estacionesmeteorolgicas),2);
B = ~any(ismissing(Inversores),2);
C = ~any(ismissing(Analizadordeplanta),2);

aux = A&B&C; %si se elimina en una tabla se elimina en todas, los datos de
%entrada deben tener el mismo número de filas

%hay días con un número de datos NaN tan elevado que apenas tienen valor,
%dado un porcentaje de datos correctos arbitrario, se eliminan estos días
for i=1:1440:length(aux) %1440 = 60*24 // datos minutales
    vec = aux(i:i+1439);
    if nnz(vec) < 0.5*length(vec) %si el numero de elementos OK es menor del X%,
        %fuera día entero
        aux(i:i+1439) = zeros(1440,1);
    end 
end

Estacionesmeteorolgicas = Estacionesmeteorolgicas(aux,:);
Inversores = Inversores(aux,:);
Analizadordeplanta = Analizadordeplanta(aux,:);

%hay problemas en septiembre, hay irradiancias 0 durante el día, quitar
%hasta el 15 de septiembre
Estacionesmeteorolgicas(1:(find(Estacionesmeteorolgicas.Datetime == '14-Sep-2020 23:59:00')),:) = [];
Analizadordeplanta(1:(find(Analizadordeplanta.Datetime == '14-Sep-2020 23:59:00')),:) = [];
Inversores(1:(find(Inversores.Datetime == '14-Sep-2020 23:59:00')),:) = [];
%%
%print data from estaciones meteorologicas
figure(4)
plot(Estacionesmeteorolgicas.Datetime,Estacionesmeteorolgicas.METEO01InclinedPyranometer1Wm2);hold on;
plot(Estacionesmeteorolgicas.Datetime,Estacionesmeteorolgicas.METEO01InclinedPyranometer2Wm2); grid on;
legend('Pyranometer1 [Wm2]', 'Pyranometer2 [Wm2]');title('Pyranometer1 VS Pyranometer2')

figure(5)
plot(Estacionesmeteorolgicas.Datetime,Estacionesmeteorolgicas.METEO01ExternalAmbientTemperature1C);hold on;
plot(Estacionesmeteorolgicas.Datetime,Estacionesmeteorolgicas.METEO01ModuleTemperature1C);
plot(Estacionesmeteorolgicas.Datetime,Estacionesmeteorolgicas.METEO01ModuleTemperature2C);
plot(Estacionesmeteorolgicas.Datetime,Estacionesmeteorolgicas.METEO01ModuleTemperature3C);grid on;
legend('Ambient Temp','1C Temp','2C Temp','3C Temp');title('Temperature comparison');

figure(6)
plot(Estacionesmeteorolgicas.Datetime,Estacionesmeteorolgicas.METEO01WindSpeed1ms);grid on;title('Wind Speed [ms]');

figure(100)
plot(Inversores.Datetime,Inversores.INVERSOR12DCPowerkW);hold on; plot(Inversores.Datetime,Inversores.INVERSOR12ActivePowerkW);
title('DC power VS AC power (Inverter12)');
plot(Inversores.Datetime, Inversores.INVERSOR12DCPowerkW-Inversores.INVERSOR12ActivePowerkW); grid on;
legend('DC power [kW]','AC power [kW]','DC power - AC power [kW]');
 
%% Pablo's tool

close all
% cd 'C:\Users\migue\Documents\Universidad\EECPS Master\TFM\Pablos_tool_MATLAB'
DatosReales_VersallesConPMAT;
cd 'VersallesMATLAB'
Versalles_TodoOp2;
% cd 'C:\Users\migue\Documents\Universidad\EECPS Master\TFM\VersallesSCADA MATLAB'
% cd 'VersallesMATLAB'
close all
%%
%datos para inversor 12 en la tabla inversores, NO TODA LA PLANTA
figure(12)
t = tiledlayout(2,1);
ax1 = nexttile;
plot(TimeVer, PDC_Inv1_2./1e3,'sg');hold on; plot(TimeVer, aSAPMResults.Pmp./1e6,'sr');
title('DC Power with limitation VS Without');legend('Real data','Pablos tool');grid on;
xlabel('time'),ylabel('Power [MW]');

ax2 = nexttile;
plot(TimeVer,aSAPMResults.Pmp./1e6 - PDC_Inv1_2./1e3,'sb');grid on;
title('Power difference');xlabel('time');ylabel('Power [MW]');
linkaxes([ax1 ax2],'x');

ToolResuls = aSAPMResults.Pmp./1e6; %Mwh
PDC_meas = PDC_Inv1_2./1e3; %Mwh
TimeVer_edit = TimeVer;

%% Post data treatment

%En el siguiente bucle se busca eliminar los días que presenten medidas
%erroneas del sensor de potencia -> la potencia medida a veces cae hasta 0 
%sin explicación
t1 = datetime(2020,9,15,9,0,0);
t2 = datetime(2020,9,15,17,0,0);

while (t2 < Estacionesmeteorolgicas.Datetime(end,1))
    tadd1 = 0; tadd2 = 0;
    %a veces la fecha que se busca fue eliminada, si es así, desplaza un
    %minuto
    while (isempty(find(TimeVer_edit == (t1+tadd1))) == true)
        tadd1 = tadd1 + minutes(1); 
        if tadd1 >= hours(4)%si el día entero fue eliminado -> break
           break 
        end
    end
    while (isempty(find(TimeVer_edit == (t2+tadd2))) == true)
        tadd2 = tadd2 - minutes(1);
        if tadd2 <= hours(4)
           break 
        end
    end
    
    if any(PDC_meas(find(TimeVer_edit == (t1+tadd1)):find(TimeVer_edit == (t2+tadd2))) <= 0.04)
        PDC_meas(find(TimeVer_edit == (t1-hours(9))):find(TimeVer_edit == (t2+hours(6)+minutes(59)))) = [];
        ToolResuls(find(TimeVer_edit == (t1-hours(9))):find(TimeVer_edit == (t2+hours(6)+minutes(59)))) = [];
        TimeVer_edit(find(TimeVer_edit == (t1-hours(9))):find(TimeVer_edit == (t2+hours(6)+minutes(59)))) = [];
    end
    %el día 12 de enero se elimina un día con solo una medida mal, que
    %hacemos?
    t1 = t1 + 1;
    t2 = t2 + 1;
end

% Si la diferencia de potencia es negativa, la hacemos 0
PowerDiff = ToolResuls - PDC_meas; %MW
for i=1:length(PowerDiff)    
    if PowerDiff(i) < 0
        PowerDiff(i) = 0;
    end 
end

figure(13) %Figura después de aplicar los cambios anteriores
t = tiledlayout(2,1);
ax1 = nexttile;
plot(TimeVer_edit, PDC_meas,'sg');hold on; plot(TimeVer_edit, ToolResuls,'sr');
title('DC Power with limitation VS Without');legend('Real data','Pablos tool');grid on;
xlabel('time'),ylabel('Power [MW]');

ax2 = nexttile;
plot(TimeVer_edit,PowerDiff,'sb');grid on;
title('Power difference');xlabel('time');ylabel('Power [MW]');
linkaxes([ax1 ax2],'x');