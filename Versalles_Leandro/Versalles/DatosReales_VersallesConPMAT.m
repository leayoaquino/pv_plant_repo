%% REAL DATA
% Inversores
% Estaciones meteorológicas
TimeVer =  Inversores{:,1};

%% INVERSOR 1.2
%MODIFIED
PDC_Inv1_2 = Inversores{:,4};
VDC_Inv1_2 = Inversores{:,2};
IDC_Inv1_2 = Inversores{:,3};
PAC_Inv1_2 = Inversores{:,5};
QAC_Inv1_2 = Inversores{:,6};

% PDC_Inv1_2 = Inversores{:,17}; ORIGINAL
% VDC_Inv1_2 = Inversores{:,15};
% IDC_Inv1_2 = Inversores{:,16};
% PAC_Inv1_2 = Inversores{:,24};
% QAC_Inv1_2 = Inversores{:,25};

%% Analizador de planta

Pplant = Analizadordeplanta{:,2};
Qplant = Analizadordeplanta{:,3};
PFplant = Analizadordeplanta{:,4}; 

%% Estación meteorologia
%MODIFIED
POA_1 = Estacionesmeteorolgicas{:,2};
POA_2 = Estacionesmeteorolgicas{:,3};
MODULET_1 = Estacionesmeteorolgicas{:,6};
MODULET_2 = Estacionesmeteorolgicas{:,7};
MODULET_3 = Estacionesmeteorolgicas{:,8};
Windspeed = Estacionesmeteorolgicas{:,4};
Tamb = Estacionesmeteorolgicas{:,5};

% POA_1 = Estacionesmeteorolgicas{:,3}; ORIGINAL
% POA_2 = Estacionesmeteorolgicas{:,4};
% MODULET_1 = Estacionesmeteorolgicas{:,7};
% MODULET_2 = Estacionesmeteorolgicas{:,8};
% MODULET_3 = Estacionesmeteorolgicas{:,9};
% Windspeed = Estacionesmeteorolgicas{:,5};
% Tamb = Estacionesmeteorolgicas{:,6};

% NOCT data
effic     = 17.01                 ;
eta_c     = effic/100             ;
tau_alpha = 0.9                   ;
G_NOCT    = 800                   ; % W/m^2
% Tamb      = 25                    ;
T_a_NOCT  = 20                    ; % ºC
% v_w       = 1                     ; % m/s
T_NOCT    = 45.1                  ; % ºC

POA = (POA_1 + POA_2)/2;
MODULET = (MODULET_1 + MODULET_2 + MODULET_3)/3;
CELLT = MODULET + (POA/1000)*3;

%%
ws_filt = zeros(length(Windspeed),1);
for i=2:length(Windspeed)
    ws_filt(i) = ws_filt(i-1) * (1 - 1e-4) + Windspeed(i) * (1e-4);
end
figure()
plot(ws_filt); hold on; plot(Windspeed);
legend('WS filt','WS');
%%
% CELLT_NOCT = Tamb + (POA/G_NOCT) * (T_NOCT - T_a_NOCT) * (1 - eta_c / tau_alpha) .* (9.5 ./ (5.7 + 3.8 * Windspeed));
CELLT_NOCT = Tamb + (POA/G_NOCT) * (T_NOCT - T_a_NOCT) * (1 - eta_c / tau_alpha) .* (9.5 ./ (5.7 + 3.8 * ws_filt));
%% Cell temp calculation
a1 = -3.47;
b1 = -0.0594;
IncTcnd = 3;
a2 = -3.56;
b2 = -0.0750;
a3 = -3.58;
b3 = -0.1130;
Tm = POA.*(exp(a2+b2*Windspeed)) + Tamb;
Tcell = Tm + (POA/1000)*IncTcnd;
%%
figure(1)
plot(TimeVer,CELLT)
hold on
plot(TimeVer,Tcell)
plot(TimeVer,CELLT_NOCT)
legend('Tcell', 'Tcell calculated', 'Tcell NOCT')


%% INJECTED POWER PLOT
% Minutes = 1:size(Pinj_Inv2);
% Time = datetime(Time,"InputFormat","dd/MM/yyyy HH:mm:ss");
% plot(Time,Pinj_Inv2*10^3) % In the excel the data is in KW
% hold on
% plot(Time,PDC_Inv2)