function [IL, I0, Rs, Rsh, S, Tcell, Tm, nNsVth] = pvl_calcparams_CEC_NOCT(ModuleParameters)
% PVL_CALCPARAMS_CECmio Calculates five parameters for an IV curve using the CEC model.
%
% Input Parameters:
%   S - The effective irradiance (in W/m^2) absorbed by the module. S must be >= 0.
%      May be a vector of the same size as Tcell. Due to a division by S in the script, any S
%      value equal to 0 will be set to 1E-10.
%   Tcell - The average cell temperature of cells within a module in C.
%      Tcell must be >= -273.15. May be a vector of the same size as S.
%   ModuleParameters - a struct with parameters for the module. 
%       ModuleParameters.a_ref - modified diode ideality factor parameter at
%          reference conditions (units of V), a_ref can be calculated as 
%          a_ref = n Ns Vth, where n is the usual diode ideality factor (n),
%             Ns is the number of cells in series, and Vth is the thermal
%             voltage at STC cell temperature 298.15K.
%       ModuleParameters.IL_ref - Light-generated current (or photocurrent) 
%          in amperes at reference conditions. 
%       ModuleParameters.I0_ref - diode reverse saturation current in amperes, 
%          under reference conditions.
%       ModuleParameters.Rsh_ref - shunt resistance under reference conditions (ohms)
%       ModuleParameters.Rs_ref - series resistance under reference conditions (ohms)
%       ModuleParameters.adjust - an adjustment factor applied to the
%          reference value for the temperature coefficient for short circuit
%          current (percent)
%       ModuleParameters.alpha_sc - temperature coefficient for
%          short-circuit current at reference conditions (A/C)
%       ModuleParameters.Ns - number of cells in series (unitless)
%  
% Output:   
%   IL - Light-generated current in amperes at irradiance S and 
%      cell temperature Tcell. 
%   I0 - Diode saturation curent in amperes at irradiance S and cell temperature Tcell. 
%   Rs - Series resistance in ohms at irradiance S and cell temperature Tcell.
%   Rsh - Shunt resistance in ohms at irradiance S and cell temperature Tcell.
%   nNsVth - modified diode ideality factor at irradiance S and cell temperature
%   S - Irradiance Levels (W/m^2) for parameter sets
%   Tcell and Tm- Calculated below

% We introduce different irradiances in order to get curves for different
% conditions in the module
S = [100 200 300 400 500 600 700 800 900 1000 1100 1200 1300]; % Irradiance Levels (W/m^2) for parameter sets
% To get the cell temperature we can use the equation Tcell = Tamb + 1/U · Ginc · Alpha · (1-effic)
Tcell = zeros(1,length(S));
Tm = zeros(1,length(S));

U = 29;      % Heat transfer U-factor [W/m^2K]
Alpha = 0.9; % Absortion coeffcient of solar irradiation or identically 1 - Reflexion (typically 0.9)

effic     = ModuleParameters.effic;
eta_c     = effic/100             ;
tau_alpha = 0.9                   ;
G_NOCT    = 800                   ; % W/m^2
Tamb      = 25                    ;
T_a_NOCT  = 20                    ; % ºC
v_w       = 1                     ; % m/s
T_NOCT    = 45.1                  ; % ºC

for i=1:length(S)
    Tcell(i) = Tamb + (S(i)/G_NOCT) * (T_NOCT - T_a_NOCT) * (1 - eta_c / tau_alpha) * (9.5 / (5.7 + 3.8 * v_w));
    Tm(i) = Tamb + 1/U * S(i) * Alpha * (1-effic/100); 
    %Tcell(i) = Tm(i) + (S(i)/1000)*3;
end
% Tcell = [28 30 33 35 38 41 43 46 49 51 54 57 59]; %deg C;

p = inputParser;
p.addRequired('S',@(x) all(x>=0) & isnumeric(x) & isvector(x) );
p.addRequired('Tcell',@(x) all(x>=-273.15) & isnumeric(x) & isvector(x) );
p.addRequired('ModuleParameters', @(x) (isstruct(x)));
p.parse(S, Tcell, ModuleParameters);

S = p.Results.S(:);
Tcell = p.Results.Tcell(:);
ModuleParameters = p.Results.ModuleParameters;

a_ref=ModuleParameters.a_ref(:);
IL_ref=ModuleParameters.IL_ref(:);
I0_ref=ModuleParameters.I0_ref(:);
Rsh_ref=ModuleParameters.Rsh_ref(:);
Rs_ref=ModuleParameters.Rs_ref(:); 
adjust=ModuleParameters.adjust(:);
alpha_sc=ModuleParameters.alpha_sc(:);

VectorSizes = [numel(S), numel(Tcell), numel(a_ref), numel(IL_ref),...
    numel(I0_ref), numel(Rsh_ref), numel(Rs_ref), numel(alpha_sc)];
MaxVectorSize = max(VectorSizes);
if not(all((VectorSizes==MaxVectorSize) | (VectorSizes==1)))
    error(['Input vectors S, Tcell, and all used components of ModuleParameters must '... 
        'either be scalars or vectors of the same length.']);
end

%k=1.3806488e-23;   %Boltzman's constant in units of J/K
k = 8.617332478e-5; % Boltzmann constant in units of eV/K

Sref = 1000;          % Reference effective irradiance in W/m2
Tref_K=25+273.15;     % Reference cell temperature in Kelvin
Tcell_K=Tcell+273.15; % cell temperature in Kelvin
EgRef = 1.121;        % in eV
dEgdT = -0.00002677;  

% adjust temperature coefficient for short-circuit current
aIsc = alpha_sc .* ( 1 - adjust/100);

%These parameters (a, I_L, I_o, M, and R_sh) need to be vectors of equal
%length to the number of conditions (number of S,Tcell pairs).
E_g=EgRef.*(1+dEgdT.*(Tcell_K-Tref_K)); 
nNsVth=a_ref.*(Tcell_K./Tref_K); 
IL=S./Sref.*(IL_ref+aIsc.*(Tcell_K-Tref_K)); 
IL(S <= 0) = 0; % If there is no light then no current is produced
I0=I0_ref.*((Tcell_K./Tref_K).^3).*exp((EgRef./(k.*Tref_K))-(E_g./(k.*Tcell_K)));
I0(IL==0) = 0; % If there is no light-generated current, there is no reverse saturation current
Rsh=Rsh_ref.*(Sref./S); 
Rsh(S <= 0) = inf; % Rsh is undefined if there is no current
Rs = Rs_ref;
