
function Module = PVCoc(Tm,IRR,Isc,alfa_Isc,Isc0,Imp,betaVoc,Voc,Vmp,V,I,Ns)

% Calculation of Ix and Ixx
r = length(Tm);
C = zeros(r,11);
Ix = zeros(r,1);
Ixx = zeros(r,1);
j = 1;
[mr,nc] = size(V);

% Not necessary, but a calculation used at the beginning for Ix and Ixx
% when it was needed to get the data from PVSyst to excel
for i = 1:nc
      C(j,:) = polyfit(V(:,i),I(:,i),10);   
      Ix(j) = C(j,1)*(Voc(j)/2)^10 + C(j,2)*(Voc(j)/2)^9 + C(j,3)*(Voc(j)/2)^8 + C(j,4)*(Voc(j)/2)^7 + C(j,5)*(Voc(j)/2)^6 + C(j,6)*(Voc(j)/2)^5 + C(j,7)*(Voc(j)/2)^4 + C(j,8)*(Voc(j)/2)^3 + C(j,9)*(Voc(j)/2)^2 + C(j,10)*Voc(j)/2 + C(j,11);
      Ixx(j) = C(j,1)*((Voc(j)+Vmp(j))/2)^10 + C(j,2)*((Voc(j)+Vmp(j))/2)^9 + C(j,3)*((Voc(j)+Vmp(j))/2)^8 + C(j,4)*((Voc(j)+Vmp(j))/2)^7 + C(j,5)*((Voc(j)+Vmp(j))/2)^6 + C(j,6)*((Voc(j)+Vmp(j))/2)^5 + C(j,7)*((Voc(j)+Vmp(j))/2)^4 + C(j,8)*((Voc(j)+Vmp(j))/2)^3 + C(j,9)*((Voc(j)+Vmp(j))/2)^2 + C(j,10)*((Voc(j)+Vmp(j))/2) + C(j,11);
      j = j+1;  
end

    
%% PROCEDURE TO OBTAIN THE COEFFICIENTS FOR THE SANDIA ARRAY PERFORMANCE MODEL
% Gpoa ->  Broadband irradiance in plane of the array (W/m2), typically apyranometer
% Epoa ->  Irradiance in plane of the array (W/m2), typically a reference cell
% G0,E0 -> Irradiance at STC, 1000 W/m2
% ΔT -> Temperature difference between a cell and the module’s back surface at 1000 W/m2 (°C), typically assumed to be 3°C
G0 = 1000;
E0 = 1000;
IncT = 3;
% Average cell temperature Tc
Tc = zeros(length(Tm),1);
Tc2 = zeros(length(Tm),1);
for i = 1:length(Tm)
    Tc(i) = Tm(i) + (IRR(i)/G0)*IncT;
    Tc2(i) = (1.7*Tm(i) + (IRR(i)/G0)*IncT); %+14; %1.7
end
    
%% Short-Circuit Current (alfaIsc)
sk = 0; % Number of points to skip in case really low irradiances are introduced which disturb in the regresion 
Isc_1000 = zeros(length(Isc)-sk,1);
Tc_25 = zeros(length(Isc)-sk,1);
Tc2_25 = zeros(length(Isc)-sk,1);
for i = 1:length(Isc)-sk
    Isc_1000(i) = Isc(i+sk)*(E0/IRR(i+sk)); 
    Tc_25(i) = Tc(i+sk) - 25;
    Tc2_25(i) = Tc2(i+sk) - 25;
end
% C_alfa_Isc = polyfit(Tc_25,Isc_1000,1);
C_alfa_Isc = polyfit(Tc2_25,Isc_1000,1);
xx_alfa_Isc = linspace(0,100,500);
yy_alfa_Isc = polyval(C_alfa_Isc,xx_alfa_Isc);
% Plotting Isc_1000 vs Tc-25, and fitting a linear equation of the form y = a + bx
% % figure(2)
% % plot(Tc2_25, Isc_1000,'or');
% % % plot(Tc_25, Isc_1000,'or');
% % hold on
% % plot(xx_alfa_Isc,yy_alfa_Isc,'b');
% % title('Determination of αIsc')
% % xlabel('Tc-25') 
% % ylabel('Isc,1000') 
% % legend({'data points','y = a+bx'},'Location','northwest')
% Now record alfaIsc with units of A/ºC and âlfaIsc with units of 1/ºC
alfaIsc = C_alfa_Isc(1);
alfa_Iscm = C_alfa_Isc(1)/C_alfa_Isc(2);
%alfa_Isc = 0.00057; % 0.0006 (JINKO)  % 0.00057 (JA)  %1/ºC

%% Open Circuit Voltage (betaVoc)
% Ns -> Number of series-connected cells in a module cell-string
% k  -> Boltzmann's constant, 1.38066 * 10^-23 (J/K)
% n  -> Diode (ideality) factor for the module’s cells (dimensionless)
% q  -> Elementary charge, 1.60218 * 10^–19 (Coulomb)
% Ns = 72;
k = 1.38066 * 10^-23;
n = 1;  % first assumption
q = 1.60218 * 10^-19; 
Voc_1000 = zeros(length(Voc)-sk,1);
for i = 1:length(Voc)-sk
    Voc_1000(i) = Voc(i+sk) - (Ns*n*k*(Tc2(i+sk)+273.15)*log(IRR(i+sk)/E0))/q;
%    Voc_1000(i) = Voc(i+6) - (Ns*n*k*(Tc(i+6)+273.15)*log(IRR(i+6)/E0))/q; 
end
C_beta_Voc = polyfit(Tc2_25,Voc_1000,1);
% C_beta_Voc = polyfit(Tc_25,Voc_1000,1);
xx_beta_Voc = linspace(0,100,500);
yy_beta_Voc = polyval(C_beta_Voc,xx_beta_Voc);
% % % Plotting Voc1000 vs Tc-25 and fitting a linear equation of the form y = a + bx
% % figure(3)
% % plot(Tc2_25,Voc_1000,'or');
% % % plot(Tc_25,Voc_1000,'or');
% % hold on
% % plot(xx_beta_Voc,yy_beta_Voc,'b');
% % title('Determination of βVoc')
% % xlabel('Tc-25') 
% % ylabel('Voc,1000') 
% % legend({'data points','y = a+bx'},'Location','southwest')
% Now record betaVoc with units of V/ºC and ^betaVoc with units of 1/ºC
betaVocm = C_beta_Voc(1);
beta_Voc = C_beta_Voc(1)/C_beta_Voc(2);
% betaVoc = -0.143; %-0.144; (JINKO x SAM) %-0.161 (JA) %V/ºC

%% Current at MPP (alfaImp)
Imp_1000 = zeros(length(Imp)-sk,1);
for i = 1:length(Imp)-sk
   Imp_1000(i) = Imp(i+sk)*(E0/IRR(i+sk)); 
end
C_alfa_Imp = polyfit(Tc2_25,Imp_1000,1);
% C_alfa_Imp = polyfit(Tc_25,Imp_1000,1);
xx_alfa_Imp = linspace(0,100,500);
yy_alfa_Imp = polyval(C_alfa_Imp,xx_alfa_Imp);
% % % Plotting Imp1000 vs Tc-25 and fitting a linear equation of the form y = a + bx
% % figure(4)
% % plot(Tc2_25,Imp_1000,'or');
% % % plot(Tc_25,Imp_1000,'or');
% % hold on
% % plot(xx_alfa_Imp,yy_alfa_Imp,'b');
% % title('Determination of  αImp')
% % xlabel('Tc-25') 
% % ylabel('Imp,1000') 
% % legend({'data points','y = a+bx'},'Location','northwest')
% Now record alfaImp with units of A/ºC and âlfaImp with units of 1/ºC
alfaImp = C_alfa_Imp(1);
alfa_Imp = C_alfa_Imp(1)/C_alfa_Imp(2);

%% Voltage at MPP (betaVmp)
% InitPos = find(IRR <= 400);
% FinalPos = find(IRR <= 1000);
% LengthVmp_1000 = FinalPos(end) - InitPos(end);
% Vmp_1000 = zeros(LengthVmp_1000,1);
% Tc_25_Vmp = zeros(LengthVmp_1000,1);
% for i = 1:LengthVmp_1000
%    Vmp_1000(i) = Vmp(InitPos(end)-1+i) - (Ns*n*k*(Tc(InitPos(end)-1+i)+273.15)*log(IRR(InitPos(end)-1+i)/E0))/q; 
%    Tc_25_Vmp(i) = Tc(InitPos(end)-1+i) - 25;
% end
 Vmp_1000 = zeros(length(Vmp)-sk,1);
 for i = 1:length(Vmp)-sk
%     Vmp_1000(i) = Vmp(i+6) - (Ns*n*k*(Tc(i+6)+273.15)*log(IRR(i+6)/E0))/q; 
      Vmp_1000(i) = Vmp(i+sk) - (Ns*n*k*(Tc2(i+sk)+273.15)*log(IRR(i+sk)/E0))/q; 
 end
C_beta_Vmp = polyfit(Tc2_25,Vmp_1000,1);
% C_beta_Vmp = polyfit(Tc_25,Vmp_1000,1);
xx_beta_Vmp = linspace(0,100,500);
yy_beta_Vmp = polyval(C_beta_Vmp,xx_beta_Vmp);
% % % % Plotting Vmp1000 vs Tc-25 and fitting a linear equation of the form y = a + bx
% % % figure(5)
% % % % plot(Tc_25_Vmp,Vmp_1000,'or');
% % % % plot(Tc_25,Vmp_1000,'or');
% % % plot(Tc2_25,Vmp_1000,'or');
% % % hold on
% % % plot(xx_beta_Vmp,yy_beta_Vmp,'b');
% % % title('Determination of  βVmp')
% % % xlabel('Tc-25') 
% % % ylabel('Vmp,1000') 
% % % legend({'data points','y = a+bx'},'Location','southwest')
% Now record betaVmp with units of V/ºC and ^betaVmp with units of 1/ºC
betaVmp = C_beta_Vmp(1);
beta_Vmp = C_beta_Vmp(1)/C_beta_Vmp(2);

%% Maximum power (gammaPmp)
 Pmp_1000 = zeros(length(Imp_1000),1);
 for i = 1:length(Imp_1000)
    Pmp_1000(i) = Imp_1000(i)*Vmp_1000(i); 
 end
 C_gamma_Pmp = polyfit(Tc2_25,Pmp_1000,1);
 xx_gamma_Pmp = linspace(0,100,500);
 yy_gamma_Pmp = polyval(C_gamma_Pmp,xx_gamma_Pmp);
% % %  % Plotting Pmp1000 vs Tc-25 and fitting a linear equation of the form y = a + bx
% % %  figure(6)
% % %  plot(Tc2_25,Pmp_1000,'or');
% % %  hold on
% % %  plot(xx_gamma_Pmp,yy_gamma_Pmp,'b');
% % %  title('Determination of  γPmp')
% % %  xlabel('Tc-25') 
% % %  ylabel('Pmp,1000') 
% % %  legend({'data points','y = a+bx'},'Location','southwest')
 % Now record alfaImp with units of A/ºC and âlfaImp with units of 1/ºC
 gammaPmp = C_gamma_Pmp(1);
 gamma_Pmp = C_gamma_Pmp(1)/C_gamma_Pmp(2);

%% Effective Irradiance (Ee)
% T0  -> Temperature for reporting, typically 25°C
T0 = 25;
Ee = zeros(length(IRR)-sk,1);
for i = 1:length(IRR)-sk
    Ee(i) = Isc(i+sk) / (Isc0*(1+alfa_Isc*(Tc(i+sk)-T0))); 
end

%% Open Circuit Voltage at STC (Voco) and diode factor, n
% Tr  -> Reference Temperature for analysis, typically 50°C.
Tr = 50;
VocTr = zeros(length(Voc)-sk,1);
IndVoc = zeros(length(Voc)-sk,1);
for i = 1:length(Voc)-sk
     VocTr(i) = Voc(i+sk)-betaVoc*(Tc(i+sk)-Tr);
     IndVoc(i) = (Ns*k*(Tc(i+sk)+273.15)*log(Ee(i)))/q;
end
C_VocTr = polyfit(IndVoc,VocTr,1);
xx_VocTr = linspace(-5,1,500);
yy_VocTr = polyval(C_VocTr,xx_VocTr);
% % % % Plotting VocTr vs IndVoc and fitting a linear equation of the form y = a + bx
% % % figure(7)
% % % plot(IndVoc,VocTr,'or');
% % % hold on
% % % plot(xx_VocTr,yy_VocTr,'b');
% % % title('Determination of Vocr and diode factor, n')
% % % xlabel('Ns·k(Tc+273.15)ln(Ee)/q') 
% % % ylabel('Voc,Tr') 
% % % legend({'data points','y = a+bx'},'Location','northwest')
% Now we can find Vocr solving the linear fit at Ee = 1
Vocr = C_VocTr(2);
n = C_VocTr(1);
Voc0 = Vocr - betaVoc*(Tr - T0);

%% Maximum Power Current at STC (Imp0), C0 and C1
ImpTr = zeros(length(Imp)-sk,1);
for i = 1:length(Imp)-sk
     ImpTr(i) = Imp(i+sk)/(1+alfa_Imp*(Tc(i+sk)-Tr));
end
% IMPORTANTE QUE PASE POR 0 PARA QUE C0 Y C1 LUEGO SUMEN 1
% Use the function polyfix from  Are Mjaavatten, Telemark University College, Norway, November 2015
C_ImpTr = polyfix(Ee,ImpTr,2,0,0);
xx_ImpTr = linspace(0,1.5,500);
yy_ImpTr = polyval(C_ImpTr,xx_ImpTr);
% % % % Plotting ImpTr vs Ee and fitting a linear equation of the form y = bx + cx^2
% % % figure(8)
% % % plot(Ee,ImpTr,'or');
% % % hold on
% % % plot(xx_ImpTr,yy_ImpTr,'b');
% % % title('Determination of Impr, C0 and C1')
% % % xlabel('Ee') 
% % % ylabel('Impr,Tr') 
% % % legend({'data points','y = bx+cx^2'},'Location','northwest')
% Now we can find Impr solving the fit at Ee = 1
Impr = C_ImpTr(3)+C_ImpTr(2)+C_ImpTr(1);
% To obtain the coefficients C0 and C1 (its sume must be 1)
C0 = C_ImpTr(2)/Impr;
C1 = C_ImpTr(1)/Impr;  % To find the unitless coeffcients
% To get Imp0
Imp0 = Impr/(1+alfa_Imp*(Tr-T0));

%% Maximum Power Voltage at STC (Vmp0), C2 and C3
VmpTr = zeros(length(Vmp)-sk,1);
IndVmp = zeros(length(VmpTr)-sk,1);
for i = 1:length(Vmp)-sk
     VmpTr(i) = Vmp(i+sk) - betaVmp*(Tc(i+sk)-Tr);
     IndVmp(i) = (n*k*(Tc(i+sk)+273.15)*log(Ee(i)))/q;
end
C_VmpTr = polyfit(IndVmp,VmpTr,2);
xx_VmpTr = linspace(-0.06,0.015,500);
yy_VmpTr = polyval(C_VmpTr,xx_VmpTr);
% % Plotting VmpTr vs the independent variable and fitting a linear equation of the form y = a + bx + cx^2
% figure(9)
% plot(IndVmp,VmpTr,'or');
% hold on
% plot(xx_VmpTr,yy_VmpTr,'b');
% title('Determination of Vmpr, C2 and C3')
% xlabel('n·k(Tc+273.15)ln(Ee)/q') 
% ylabel('Vmpr,Tr') 
% legend({'data points','y = a+bx+cx^2'},'Location','northwest')
% % Now we can find Vmpr solving the linear fit at Ee = 1
Vmpr = C_VmpTr(3);
Vmp0 = Vmpr - betaVmp*(Tr - T0);
% To obtain the coefficients C0 and C1 (its sume must be 1)
C2 = C_VmpTr(2)/Ns;
C3 = C_VmpTr(1)/Ns;  % To find the unitless coeffcients

%% Ix at STC (Ix0), C4 and C5 
IxTr = zeros(length(Ix)-sk,1);
for i = 1:length(Ix)-sk
     IxTr(i) = Ix(i+sk)/(1+((alfa_Isc+alfa_Imp)/2)*(Tc(i+sk)-Tr));
end
% IMPORTANTE QUE PASE POR 0 PARA QUE C4 Y C5 LUEGO SUMEN 1
% Use the function polyfix from  Are Mjaavatten, Telemark University College, Norway, November 2015
C_IxTr = polyfix(Ee,IxTr,2,0,0);
xx_IxTr = linspace(0,1.5,500);
yy_IxTr = polyval(C_IxTr,xx_IxTr);
% % % Plotting ImpTr vs Ee and fitting a linear equation of the form y = bx + cx^2
% % figure(10)
% % plot(Ee,IxTr,'or');
% % hold on
% % plot(xx_IxTr,yy_IxTr,'b');
% % title('Determination of Ixr, C4 and C5')
% % xlabel('Ee') 
% % ylabel('Ix,Tr') 
% % legend({'data points','y = bx+cx^2'},'Location','northwest')
% Now we can find Impr solving the fit at Ee = 1
Ixr = C_IxTr(3)+C_IxTr(2)+C_IxTr(1);
% To obtain the coefficients C0 and C1 (its sume must be 1)
C4 = C_IxTr(2)/Ixr;
C5 = C_IxTr(1)/Ixr;  % To find the unitless coeffcients
% To get Ix0
Ix0 = Ixr/(1+((alfa_Isc+alfa_Imp)/2)*(Tr-T0));

%% Ixx at STC (Ixx0), C6 and C7
IxxTr = zeros(length(Ixx)-sk,1);
for i = 1:length(Ixx)-sk
     IxxTr(i) = Ixx(i+sk)/(1+((alfa_Imp)/2)*(Tc(i+sk)-Tr));
end
% IMPORTANTE QUE PASE POR 0 PARA QUE C6 Y C7 LUEGO SUMEN 1
% Use the function polyfix from  Are Mjaavatten, Telemark University College, Norway, November 2015
C_IxxTr = polyfix(Ee,IxxTr,2,0,0);
xx_IxxTr = linspace(0,1.5,500);
yy_IxxTr = polyval(C_IxxTr,xx_IxxTr);
% % % % Plotting ImpTr vs Ee and fitting a linear equation of the form y = bx + cx^2
% % % figure(11)
% % % plot(Ee,IxxTr,'or');
% % % hold on
% % % plot(xx_IxxTr,yy_IxxTr,'b');
% % % title('Determination of Ixxr, C6 and C7')
% % % xlabel('Ee') 
% % % ylabel('Ixx,Tr') 
% % % legend({'data points','y = bx+cx^2'},'Location','northwest')
% Now we can find Impr solving the fit at Ee = 1
Ixxr = C_IxxTr(3)+C_IxxTr(2)+C_IxxTr(1);
% To obtain the coefficients C0 and C1 (its sume must be 1)
C6 = C_IxxTr(2)/Ixxr;
C7 = C_IxxTr(1)/Ixxr;  % To find the unitless coeffcients
% To get Ix0
Ixx0 = Ixxr/(1+((alfa_Imp)/2)*(Tr-T0));

%% Creation of an struct array for the data
Module.name = 'Jinko JKM 330 PP-72-V';
Module.vintage = 2016;
Module.material = 'Si-poly';
Module.area = 1.752;
Module.AlphaIsc = alfa_Isc;
Module.AlphaAIsc = alfaIsc;
Module.AlphaImp = alfa_Imp;
Module.AlphaAImp = alfaImp;
Module.Isc0 = Isc0;
Module.Imp0 = Imp0;
Module.Voc0 = Voc0;
Module.Vmp0 = Vmp0;
Module.BetaVoc = betaVoc;
Module.BetaVmp = betaVmp;
Module.mBetaVoc = 0;
Module.mBetaVmp = 0;
Module.Ns = Ns;
Module.Np = 1;
% Module.delT = 3;
% Module.fd = 1;
Module.n = n;
% Module.Ix = Ix;
% Module.Ixx = Ixx;
Module.Ix0 = Ix0;
Module.Ixx0 = Ixx0;
Module.c = [C0 C1 C2 C3 C4 C5 C6 C7];


end