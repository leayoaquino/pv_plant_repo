This folder contains the data contioning for Versalles PV plant. Missing data
has been replaced and repeated data has been removed. A battery is selected
based on stats but still in a handmade way. The battery is test during 2 arbitrary
days. The code is not optimized. The MATLAB script calls Pablo's tool by changing
the folder. The code will not run if these lines are not modifed based on your 
files organization.

The folder contains:

	- VersallesDataConditioningAndBatterySelection.m : file where the previously 
commented tasks are done.
	- DatosReales_VersallesConPMAT.m : file that prepares the data for Pablo's tool.
It is necessary to run it before the tool.
	- [].mat : some information files.