%% REAL DATA
% Inversores
% Estaciones meteorológicas
CombinaCSV

TimeVer =  Datetime;

%% INVERSOR 1.2
%MODIFIED
PDC_Inv1_1 = Inv1_ActivePower_kW;
% VDC_Inv1_1 = OruroF20405.IS11_INV1_DCVOLTAGEDCPLUSTODCMINUS;
% IDC_Inv1_1 = OruroF20405.IS11_INV1_DCCURRENTTOTAL;
% PAC_Inv1_1 = OruroF20405.IS11_INV1_ACTIVEPOWERTOTAL;
% QAC_Inv1_1 = OruroF20405.IS11_INV1_REACTIVEPOWERTOTAL;

% PDC_Inv1_2 = OruroF20405.IS11_INV2_DCPOWERTOTAL;
% VDC_Inv1_2 = OruroF20405.IS11_INV2_DCVOLTAGEDCPLUSTODCMINUS;
% IDC_Inv1_2 = OruroF20405.IS11_INV2_DCCURRENTTOTAL;
% PAC_Inv1_2 = OruroF20405.IS11_INV2_ACTIVEPOWERTOTAL;
% QAC_Inv1_2 = OruroF20405.IS11_INV2_REACTIVEPOWERTOTAL;




%% Analizador de planta

% Pplant = Analizadordeplanta{:,2};
% Qplant = Analizadordeplanta{:,3};
% PFplant = Analizadordeplanta{:,4}; 

%% Estación meteorologia
%MODIFIED
POA_1 = Met_Irradiance1;
POA_2 = Met_Irradiance2;
MODULET_1 = Met_SurfaceTemp1;
MODULET_2 = Met_SurfaceTemp2;
MODULET_3 = Met_SurfaceTemp3;
MODULET_4 = Met_SurfaceTemp4;
% Windspeed = OruroF20405.MET2__WINDSPEED; %está a unos4-5m
% Windspeed1 = OruroF20405.MET2__WINDSPEED1; %está a unos4-5m
Tamb = Met_Temperature;
% Tamb1 = OruroF20405.MET2__AIRTEMPERATURE1;

% Windspeed=(Windspeed+Windspeed1)/2;
% Tamb=(Tamb+Tamb1)/2;
POA=(POA_1+POA_2)/2;
MODULET=(MODULET_1+MODULET_2+MODULET_3+MODULET_4)/4;


for i=1:1:length(MODULET)
   
    if(MODULET(i)>70)
        MODULET(i)=70;
    end
    
        if(MODULET(i)<-25)
        MODULET(i)=-25;
    end
end

CELLT = MODULET + (POA/1000)*3;


%Correccion Viento. El piranometro está colocado a una altura entre 4-5m
%https://www.agr2.es/es/noticias-agr2/calculando-la-velocidad-del-viento


% for(i=1:1:length(Windspeed))
%     
%     if isnan(Windspeed(i))
%         Windspeed(i)=Windspeed(i-1);
%     end
% end
% a_v=0.2; %Zona rústica = 0,2
% Windspeed_10m=Windspeed/((4.5/10)^a_v);
% Windspeed_2m = Windspeed_10m*(2/10)^a_v;
% 
% %%
% % NOCT data
% effic     = 17.01                 ;
% eta_c     = effic/100             ;
% tau_alpha = 0.9                   ;
% G_NOCT    = 800                   ; % W/m^2
% % Tamb      = 25                    ;
% T_a_NOCT  = 20                    ; % ºC
% % v_w       = 1                     ; % m/s
% T_NOCT    = 45.1                  ; % ºC
% CELLT_NOCT = Tamb + (POA/G_NOCT) * (T_NOCT - T_a_NOCT) * (1 - eta_c / tau_alpha) .* (9.5 ./ (5.7 + 3.8 * Windspeed_2m));
% 
% %% Cell temp calculation
% a1 = -3.47;
% b1 = -0.0594;
% IncTcnd = 3;
% a2 = -3.56;
% b2 = -0.0750;
% a3 = -3.58;
% b3 = -0.1130;
% Tm = POA.*(exp(a2+b2*Windspeed_2m)) + Tamb;
% Tcell = Tm + (POA/1000)*IncTcnd;
% 
% 
% a1 = -3.47;
% b1 = -0.0594;
% 
% a2 = -3.56;
% b2 = -0.0750;
% IncTcnd = 3;
% 
% Tamb(isnan(Tamb))=0;
% POA(isnan(POA))=0;
% Tcell_PVL = pvl_sapmcelltemp(POA, 1000, a2, b2, Windspeed_2m, Tamb, IncTcnd);
% 
% 
% % % figure(1)
% % % plot(TimeVer,CELLT)
% % % hold on
% % % plot(TimeVer,Tcell_PVL)
% % % plot(TimeVer,CELLT_NOCT)
% % % plot(TimeVer,MODULET)
% % % legend('Tcell', 'Tcell calculated','NOCT','MEAS')


clear('ESTUDIO*')
clear('Inv*')
clear('Met_*')
clear('MODULET_*')
clear('POA_*')
% clear('POA_*')