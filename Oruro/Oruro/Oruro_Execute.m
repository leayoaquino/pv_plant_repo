clear;clc;close all;
load DatosCrudos.mat
% load Analizadordeplanta_onlyneededinputs.mat
% load Estacionesmeteorologicas_onlyneededinputs.mat

 
%% PVEM Call

% close all
% cd 'C:\Users\migue\Documents\Universidad\EECPS Master\TFM\Pablos_tool_MATLAB'
ExtractData_Oruro;
% addpath('/Volumes/Datos/OneDrive - Universidad de Oviedo/ProyectosEmpresa/2021/TSK Solar 2 years/PVEM/PVEM')  
% cd 'C:\Users\migue\Documents\Universidad\EECPS Master\TFM\Pablos_tool_MATLAB'
PVModule_definition;
% Module,POA,Windspeed_2m,Tamb,ArrayTemp,TimeVer
% [Imp,Vmp,Pmp]=PVEM(Module,POA,Windspeed_2m,Tamb,Tcell_PVL,TimeVer);

inverter_definition;

Windspeed_2m=3*ones(1,length(Tamb));
[Imp,Vmp,Pmp,Pac]=PVEM(Module,Ns,Np,POA,Windspeed_2m,CELLT,inverter);
Pac_pre=Pac;


%% SMA altitude derating

SMA_derating;
%% 

% figure
% % plot(TimeVer,(Vmp)./(VDC_Inv1_2))
% plot(Vmp);hold on;plot((VDC_Inv1_2))
% hold on
% 
% figure(2)
% % plot(TimeVer,(Imp)./(IDC_Inv1_2))
% plot(Imp);hold on;plot((IDC_Inv1_2))
% hold on

figure(3)
% plot(TimeVer,(Imp)./(IDC_Inv1_2))
% plot(Pmp/1e3);hold on;plot((PDC_Inv1_2));plot((PDC_Inv1_1))
plot(Pac/1e3);hold on;plot((PDC_Inv1_1))

% plot(Pac/1e3);hold on;plot((PDC_Inv1_1))
% plot(Pac./1e3./PDC_Inv1_1)
hold on
legend('PVEM','meas')


% % % To check temperature Derating                
% figure;
% subplot(211)
% plot(Pac/1e3);hold on;plot(Pac_pre/1e3);plot((PDC_Inv1_1))
% grid on
% subplot(212)
% plot(Tamb)
% grid on

% cd 'VersallesMATLAB'
