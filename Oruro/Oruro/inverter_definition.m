% %% Inverter
load('CECInverters.mat')


Name = ('2750-EV'); % Introduce el nombre del mÃ³dulo

% k = (strcmp({SandiaInverterDatabase.ModelNumber},Name)); % Encuentra el mÃ³dulo en el catÃ¡logo
% k=contains(SandiaInverterDatabase.ModelNumber,"2750-EV");
k=contains(CECInverters.Name,Name);
index = find(k); % PosiciÃ³n del modulo dentro del catalogo

% k=contains(CECInverters.Name,"2750-EV");


% inverter=SandiaInverterDatabase(index,:);

inverter.name = 'Sunny Central 2750-EV_4000';   %SAM PARAMETERS
inverter.vac = CECInverters(index,:).Vac;%
inverter.Pac0 = CECInverters(index,:).Paco;%2.6e+06;%2650e3; %2600e3;
inverter.Pdc0 = CECInverters(index,:).Pdco;%2.677e+06;%2750e3; %2.677e6;
inverter.Vdc0 = CECInverters(index,:).Vdco;%;
inverter.Ps0 = CECInverters(index,:).Pso;%11.56284e3;
inverter.C0 = CECInverters(index,:).C0;%-8.01208e-09;
inverter.C1 = CECInverters(index,:).C1;%9.54256e-06;
inverter.C2 = CECInverters(index,:).C2;%0.000911;
inverter.C3 = CECInverters(index,:).C3;%0.000214;
inverter.Pnt = CECInverters(index,:).Pnt;%780;


inverter.Vdcmax= CECInverters(index,:).Vdcmax;
inverter.Idcmax= CECInverters(index,:).Idcmax;
inverter.MPPTLow= CECInverters(index,:).Mppt_low;
inverter.MPPTHi= CECInverters(index,:).Mppt_high;
inverter.LibraryType= {'SandiaInverter'};
inverter.LibraryName= {'Sandia Inverters'};