function [I_MPP,V_MPP,P_MPP,ACPower]=PVEM(Module,Ns,Np,POA,Windspeed_2m,ArrayTemp,inverter)


%% Predicting IV curve points

% We call the function pvl_calcparams_CECmio to calculate the desired IV
% curves and data for different T and E in the module. 
% Results are saved in IVResult
% [IL, I0, Rs, Rsh, S, Tcell, Tm, a] = pvl_calcparams_CECmio(Module);

% We introduce different irradiances in order to get curves for different
% conditions in the module
S = [100 200 300 400 500 600 700 800 900 1000 1100 1200 1300]; % Irradiance Levels (W/m^2) for parameter sets
% To get the cell temperature we can use the equation Tcell = Tamb + 1/U · Ginc · Alpha · (1-effic)
Tcell = zeros(1,length(S));
Tm = zeros(1,length(S));
Tamb = 25;
U = 29;      % Heat transfer U-factor [W/m^2K]
Alpha = 0.9; % Absortion coeffcient of solar irradiation or identically 1 - Reflexion (typically 0.9)
effic = Module.effic;
for i=1:length(S)
    Tm(i) = Tamb + 1/U * S(i) * Alpha * (1-effic/100); 
    Tcell(i) = Tm(i) + (S(i)/1000)*3;
end

a2 = -3.56;
b2 = -0.0750;
IncTcnd = 3;
Tamb_calc=25*ones(1,length(S));
Windspeed_2m_calc=ones(1,length(S));

% Tcell_PVL_calc = pvl_sapmcelltemp(S,1000,a2,b2, Windspeed_2m_calc, Tamb_calc, IncTcnd);

% Tcell_PVL_calc = pvl_sapmcelltemp(POA,1000,a2,b2, Windspeed_2m, Tamb, IncTcnd);
Tcell_PVL_calc = pvl_sapmcelltemp(S,1000,a2,b2, mean(Windspeed_2m)*ones(1,length(S)), Tamb*ones(1,length(S)), IncTcnd);


% [IL, I0, Rs, Rsh, S, Tcell, Tm, a] = pvl_calcparams_CECmio(Module);
[IL, I0, Rs, Rsh, a] = pvl_calcparams_CEC(S,Tcell,Module);
% [IL, I0, Rs, Rsh, a] = pvl_calcparams_CEC(S, Tcell_PVL_calc, Module);

% [IL, I0, Rs, Rsh, a] = pvl_calcparams_CEC(S, Tcell_PVL_calc, Module);
% [IL, I0, Rs, Rsh, a] = pvl_calcparams_CEC(POA, Tcell_PVL_calc, Module);


% [IL, I0, Rs, Rsh, nNsVth] = pvl_calcparams_PVsyst(S, Tcell, alpha_isc, ModuleParameters);
NumPoints = 1000;
[IVResult] = pvl_singlediode(IL, I0, Rs, Rsh, a, NumPoints);

% % Plots of the curves
% figure(1)
% for i=1:length(S)
%     plot(IVResult.V(i,:),IVResult.I(i,:))
%     hold on
%     scatter(IVResult.Vmp(i),IVResult.Imp(i),'filled')
%     text(2,IVResult.Isc(i)+0.3,[num2str(S(i)) ' W/m^2 at ' num2str(Tcell(i)) ' ºC'])
% end
% xlabel('Voltage (V)')
% ylabel('Current (A)')
% title('IV Curve from CEC Single Diode Model','FontSize',14)
% ylim([0 14])


%% NOW WE ARE GOING TO START WITH THE EMULATING OF THE POWER

%% Transfer the obtained data 
Tm = Tcell';
IRR = S';
Isc = IVResult.Isc;
Imp = IVResult.Imp;
Voc = IVResult.Voc;
Vmp = IVResult.Vmp;
V = IVResult.V';
I = IVResult.I';

%% Irradiance and Temperature for a year
Ee = POA/1000;
% ArrayTemp = MODULET;       
% ArrayTemp = CELLT;   
% ArrayTemp =CELLT_NOCT;

% ArrayTemp =Tcell_PVL;

ModuleParametersJ = PVCoc(Tm,IRR,Isc,Module.alpha_sc,Module.Isc0,Imp,Module.beta_oc,Voc,Vmp,V,I,Module.Ns);

%% Comparison
% Im = [ ModuleParametersJ.Isc0 ModuleParametersJ.Ix0 ModuleParametersJ.Imp0 ModuleParametersJ.Ixx0 0 ];
% Vm = [ 0 ModuleParametersJ.Voc0/2 ModuleParametersJ.Vmp0 (ModuleParametersJ.Voc0+ModuleParametersJ.Vmp0)/2 ModuleParametersJ.Voc0 ];
% % Id = [ Isc0 Imp0 0 ]; 
% % % Id = [ 9.14 8.68 0 ]; %JINKO
% % Vd = [ 0 Vmp0 Voc0]; 
% % % Vd = [ 0 38.1 46.9 ]; %JINKO
% % % Cm = polyfix(Vm,Im,30,0,Isc0);
% % Cd = polyfix(Vd,Id,20,0,Id(1));
% % % xxm = linspace(0,48,500);
% % % yym = polyval(Cm,xxm);
% % xxd = linspace(0,48,500);
% % yyd = polyval(Cd,xxd);
% % 
% % % Cubic Spline data interpolation
% % tm = 1:numel(Vm);
% % xym = [Vm;Im];
% % ppm = spline(tm,xym);
% % tInterpm = linspace(1,numel(Vm));
% % xyInterpm = ppval(ppm, tInterpm);

% % Show the result
% figure(12)
% plot(Vm,Im,'xr')
% hold on
% 
% plot(xyInterpm(1,:),xyInterpm(2,:),'r')
% hold on
% 
% plot(Vd,Id,'og');
% hold on
% plot(xxd,yyd,'g');
% title('Comparation between points from the datasheet and the ones obtained in IV curve form')
% xlabel('V (V)') 
% ylabel('I (A)')
% legend({'Resulted points','Interpolation','Datasheet points','Interpolation'})
% ylim([0 15])

%% DC

% PDClimit = 1.456e+06 + 1.456e+06 *(1-0.986);

% PDClimit = inf;

ModuleParametersJ.Voc0 = Module.Voc0;
ModuleParametersJ.Vmp0 = Module.Vmp0;
ModuleParametersJ.Imp0 = Module.Imp0;

Ee(isnan(Ee))=0; % Set any NaNs to zero
LID=1.8/100; %PVSyst is 1.8%. Datasheet is up to 2.5%
Ee=Ee*(1-LID);

mSAPMResults = pvl_sapm(ModuleParametersJ, Ee, ArrayTemp);
aSAPMResults.Vmp = Ns*mSAPMResults.Vmp;                      
aSAPMResults.Imp = Np*mSAPMResults.Imp;

% % aSAPMResults.Pmp = zeros(size(aSAPMResults.Vmp,1),1);
% aSAPMResults.Pmp = zeros(length(aSAPMResults.Vmp),1);
% 
% % Power limitation
% for i = 1:length(aSAPMResults.Vmp)
% if (aSAPMResults.Vmp(i)*aSAPMResults.Imp(i))>PDClimit   % If the actual power is above the limit for each inverter
%     aSAPMResults.Imp(i) = PDClimit/aSAPMResults.Vmp(i); % Calculate the new DC current with the limit P and unvariable V
%     aSAPMResults.Pmp(i) = aSAPMResults.Vmp(i) .* aSAPMResults.Imp(i);
% else 
%     aSAPMResults.Imp(i) = Np  *mSAPMResults.Imp(i);     % If the actual power is below the limit for each inverter, keep it
%     aSAPMResults.Pmp(i) = aSAPMResults.Vmp(i) .* aSAPMResults.Imp(i);
% end
% end

aSAPMResults.Pmp=aSAPMResults.Imp.*aSAPMResults.Vmp;

% DC power losses, assume a 1%, it should be introduced by yourself
Ploss = 1.26/100;
ShadingLoss=0.86/100;%1.56
IAM_Loss=2.56/100;

% aSAPMResults.Vmp=aSAPMResults.Vmp*(1-ShadingLoss);
Total_Loss=Ploss+ShadingLoss+IAM_Loss;
% Total_Loss=Ploss;

% aSAPMResults.Pmp = aSAPMResults.Pmp - aSAPMResults.Pmp.*Ploss;
aSAPMResults.Pmp = aSAPMResults.Pmp - aSAPMResults.Pmp.*Total_Loss;

% plot(TimeVer,aSAPMResults.Vmp-VDC_Inv1_2)
% plot(TimeVer,(aSAPMResults.Pmp/10^6)./(PDC_Inv1_2/10^3))
P_MPP=aSAPMResults.Pmp;

I_MPP= aSAPMResults.Imp;
V_MPP= aSAPMResults.Vmp;

% figure('OuterPosition',[100 100 600 800])
% tiledlayout(3,1) % subplot(3,1,1)
% % First plot
% ax1 = nexttile;
% plot(TimeVer,aSAPMResults.Pmp/10^6,'-sr')
% hold on
% plot(TimeVer,PDC_Inv1_2/10^3,'-sg')
% legend('Pmp','Real data DC power')
% %xlabel('Seconds of the day')
% ylabel('DC Power (MW)')
% title('Versalles, DC Power','FontSize',14)
% % Second plot
% ax2 = nexttile; % subplot(3,1,2)
% plot(TimeVer,aSAPMResults.Imp,'-ok')
% hold on
% plot(TimeVer,IDC_Inv1_2,'-sg')
% legend('Imp','Excel data DC current')
% %xlabel('Seconds of the day')
% ylabel('DC Current (A)')
% title('Versalles, Current','FontSize',12)
% % Third plot
% ax3 = nexttile; % subplot(3,1,3)
% plot(TimeVer,aSAPMResults.Vmp,'-xk')
% hold on
% plot(TimeVer,VDC_Inv1_2,'-sg')
% legend('Vmp','Excel data DC voltage')
% xlabel('Minutes') 
% ylabel('DC Volatage (V)')
% title('Versalles, Voltage','FontSize',12)
% 
% linkaxes([ax1 ax2 ax3],'x')


%% DC to AC Conversion
%   Nsp = Np; % Number of parallel strings
%   Ni = 1;    % Number of inverters
%   ACPower = pvl_snlinverter(SMAinverter, aSAPMResults.Vmp, aSAPMResults.Pmp);   
  ACPower = pvl_snlinverter(inverter,V_MPP,P_MPP);   

  ACPower=ACPower';

  % Apply losses in AC wiring and Transformers
  AC_CableLosses=0.1/100;
  TransformerLosses=1.52/100;
  LossesAC=AC_CableLosses+TransformerLosses;

  ACPower = ACPower*(1-LossesAC);
  
end

