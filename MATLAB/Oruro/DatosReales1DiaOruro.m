%% REAL DATA
% 1 DÍA
% MAYO 2021
% Inversor 11_1 Y 11_2 y unas horas solo

[num2,Time2,both2] = xlsread('Oruro F2 04_05.xlsx','A3:A843');
Time = Time2;

AirT_1 = xlsread('Oruro F2 04_05.xlsx','CV3:CV843'); 
AirT_2 = xlsread('Oruro F2 04_05.xlsx','DE3:DE843'); 
Windspeed1 = xlsread('Oruro F2 04_05.xlsx','DD3:DD843'); 
Windspeed2 = xlsread('Oruro F2 04_05.xlsx','DM3:DM843'); 

SURFACET_1_1 = xlsread('Oruro F2 04_05.xlsx','DA3:DA843'); 
SURFACET_1_2 = xlsread('Oruro F2 04_05.xlsx','DB3:DB843'); 
SURFACET_2_1 = xlsread('Oruro F2 04_05.xlsx','DJ3:DJ843'); 
SURFACET_2_2 = xlsread('Oruro F2 04_05.xlsx','DK3:DK843'); 
POA_1 = xlsread('Oruro F2 04_05.xlsx','CZ3:CZ843'); 
POA_2 = xlsread('Oruro F2 04_05.xlsx','DI3:DI843'); 
Inv1T = xlsread('Oruro F2 04_05.xlsx','AD3:AD843');
Inv2T = xlsread('Oruro F2 04_05.xlsx','CA3:CA843');

QAC_Inv11_1 = xlsread('Oruro F2 04_05.xlsx','X3:X843');
QAC_Inv11_2 = xlsread('Oruro F2 04_05.xlsx','BU3:BU843');
PAC_Inv11_1 =  xlsread('Oruro F2 04_05.xlsx','Y3:Y843');
PAC_Inv11_2 =  xlsread('Oruro F2 04_05.xlsx','BV3:BV843');
SAC_Inv11_1 = xlsread('Oruro F2 04_05.xlsx','W3:W843');
SAC_Inv11_2 = xlsread('Oruro F2 04_05.xlsx','BT3:BT843');
Pinj_Inv11_1 = xlsread('Oruro F2 04_05.xlsx','H3:H843');
Pinj_Inv11_2 = xlsread('Oruro F2 04_05.xlsx','BE3:BE843');
IDC_Inv11_1 = xlsread('Oruro F2 04_05.xlsx','G3:G843');
IDC_Inv11_2 = xlsread('Oruro F2 04_05.xlsx','BD3:BD843');
VDC_Inv11_1 = xlsread('Oruro F2 04_05.xlsx','I3:I843');
VDC_Inv11_2 = xlsread('Oruro F2 04_05.xlsx','BF3:BF843');

VAC_Inv11_1_LL12 = xlsread('Oruro F2 04_05.xlsx','K3:K843');
VAC_Inv11_1_LL23 = xlsread('Oruro F2 04_05.xlsx','L3:L843');
VAC_Inv11_1_LL31 = xlsread('Oruro F2 04_05.xlsx','M3:M843');

VAC_Inv11_2_LL12 = xlsread('Oruro F2 04_05.xlsx','BH3:BH843');
VAC_Inv11_2_LL23 = xlsread('Oruro F2 04_05.xlsx','BI3:BI843');
VAC_Inv11_2_LL31 = xlsread('Oruro F2 04_05.xlsx','BJ3:BJ843');

PFE_Inv11_1 = xlsread('Oruro F2 04_05.xlsx','S3:S843');
PFE_Inv11_2 = xlsread('Oruro F2 04_05.xlsx','BP3:BP843');

length1 = size(Inv1T);



%% EXTRACTED VARIABLES

POA_avg = (POA_1 + POA_2)/2;
% AirtT_avg = (AirT_1+AirT_2)/2;
SURFACET_1_avg = (SURFACET_1_1 + SURFACET_1_2)/2;
SURFACET_2_avg = (SURFACET_2_1 + SURFACET_2_2)/2;
SURFACET_avg = (SURFACET_1_avg + SURFACET_2_avg)/2;
Windspeed_avg = (Windspeed1+Windspeed2)/2;
AirT_avg = (AirT_1+AirT_2)/2;

%% INJECTED POWER PLOT
Minutes = 1:size(Inv1T);
Time = datetime(Time,"Format","dd-MMM-yyyy HH:mm:ss");
%  figure(1)
%  plot(Time,Pinj_Inv11_1*10^3) % In the excel the data is in KW

tiledlayout(2,1) % subplot(3,1,1)
% First plot
% ax1 = nexttile;
PF_Inv11_1 = PAC_Inv11_1./sqrt(PAC_Inv11_1.^2+QAC_Inv11_1.^2);
% plot(Time,PFE_Inv11_1);
% hold on
% plot(Time,PF_Inv11_1);
% legend('PF excel', 'PW calculated')
%  xlabel('Time in minutes')
%  ylabel('cos(phi)')
%  title('Inverter 11_1 ','FontSize',14)
%  ylim([-1.5 1.5])
% Second plot
% ax2 = nexttile; % subplot(3,1,2)
PF_Inv11_2 = PAC_Inv11_2./sqrt(PAC_Inv11_2.^2+QAC_Inv11_2.^2);
% plot(Time,PFE_Inv11_2);
% hold on
% plot(Time,PF_Inv11_2);
% legend('PF excel', 'PW calculated')
%  xlabel('Time in minutes')
%  ylabel('cos(phi)')
%  title('Inverter 11_2 ','FontSize',14)
%  ylim([-1.5 1.5])
% linkaxes([ax1 ax2],'x')

%% Cell temp calculation
a1 = -3.47;
b1 = -0.0594;
IncTcnd = 3;
a2 = -3.56;
b2 = -0.0750;
a3 = -3.58;
b3 = -0.1130;
Tm = POA_avg.*(exp(a3+b3*Windspeed_avg))+AirT_avg;
Tcell = Tm + (POA_avg/1000)*IncTcnd;

figure(1)
plot(Time,SURFACET_avg)
hold on
plot(Time,Tcell)
legend('Tsurface', 'Tcell calculated')