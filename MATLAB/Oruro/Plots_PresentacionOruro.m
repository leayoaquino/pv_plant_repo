% SI QUEREMOS COMPARAR PRIMERO RUNEAR EL SCRIPT DE DATOS REALES -
% DatosReales1DiaOruro
% También necesario para los datos de temperatura e irradiancia
%% PV MODULE
% First of all we need to know the module we are going to use or try

load('CECModuleSAM.mat')       % Huge datasheet with modules with CEC parameters
% JKM330PP-72-V  # 10714
% Module = CECModuleSAM(10714);
% If we don't have the desired moudule in the datasheet we should introduce
% its characteristics
% Ind this case we have it, these values are the same as in Module = CECModuleSAM(10714);
Module.name = 'Jinko Solar JKM320PP-72';
Module.a_ref = 1.87478;
Module.IL_ref = 9.23276;
Module.I0_ref = 1.2592e-10;
Module.Rsh_ref = 2754.66;
Module.Rs_ref = 0.406344;
Module.alpha_sc = 0.005694;
Module.adjust = 9.24109;
% We need to include also the efficiency that appears on the module
% datasheet
Module.effic = 17.01; % in %


%% Predicting IV curve points

% We call the function pvl_calcparams_CEC to calculate the desired IV
% curves and data for different T and E in the module. 
% Results are saved in IVResult
[IL, I0, Rs, Rsh, S, Tcell, Tm, a] = pvl_calcparams_CECmio(Module);
NumPoints = 1000;
[IVResult] = pvl_singlediode(IL, I0, Rs, Rsh, a, NumPoints);

% Plots of the curves
figure(1)
for i=1:length(S)
    plot(IVResult.V(i,:),IVResult.I(i,:))
    hold on
    scatter(IVResult.Vmp(i),IVResult.Imp(i),'filled')
    text(2,IVResult.Isc(i)+0.3,[num2str(S(i)) ' W/m^2 at ' num2str(Tcell(i)) ' ºC'])
end
xlabel('Voltage (V)')
ylabel('Current (A)')
title('IV Curve from CEC Single Diode Model','FontSize',14)
ylim([0 14])

%% NOW WE ARE GOING TO START WITH THE EMULATING OF THE POWER

%% Transfer the obtained data 
Tm = Tcell';
IRR = S';
Isc = IVResult.Isc;
Imp = IVResult.Imp;
Voc = IVResult.Voc;
Vmp = IVResult.Vmp;
V = IVResult.V';
I = IVResult.I';

%% Inverter
% Different databases for the inverter
load('CECInverterSAM.mat')
load('SandiaInverterDatabaseSAM2014.1.14.mat')
load('CECInverterDatabaseSAM2015.6.30.mat')
load('DriesseInverterDatabaseSAM2013.10.mat')
% If we don't want to use an inverter from the database we can introduce
% its parameters. For C coefficients we can use SAM
SMAinverter.name = 'Sunny Central 2750-EV_4000';   %SAM PARAMETERS
SMAinverter.vac = 600;
SMAinverter.Pac0 = 330*24*330*0.97;%2650e3; %2600e3;
SMAinverter.Pdc0 = 330*24*330;%2750e3; %2.677e6;
SMAinverter.Vdc0 = 957;
SMAinverter.Ps0 = 11.56284e3;
SMAinverter.C0 = -8.01208e-09;
SMAinverter.C1 = 9.54256e-06;
SMAinverter.C2 = 0.000911;
SMAinverter.C3 = 0.000214;
SMAinverter.Pnt = 780;
% SMAinverter.Vdcmax = 1200;
% SMAinverter.Idcmax = 2797.284;
% SMAinverter.MPPTLow = 875;
% SMAinverter.MPPTHi = 1200;
% SMAinverter.LibraryType = {'Sandia Inverter'};
% SMAinverter.LibraryName = {'Sandia Inverters'};

%% Define array configuration
Ns = 24;   % Number of modules in series
Np = 330; % Number of parallel strings

%% Irradiance and Temperature for a year
Ee = POA_avg/1000;
ArrayTemp = SURFACET_avg;       % Surface Temperature

% prompt = 'Type the Isc0 of the PV module: ';
% Isc0 = input(prompt);
Isc0 = 9.14;
% prompt = 'Type the Voc0 of the PV module: ';
% Voc0 = input(prompt);
Voc0 = 46.9;
% prompt = 'Type the Imp0 of the PV module: ';
% Imp0 = input(prompt);
Imp0 = 8.74;
% prompt = 'Type the Vmp0 of the PV module: ';
% Vmp0 = input(prompt);
Vmp0 = 37.8;
BetaVoc =  -0.143;   %-0.144; (JINKO x SAM) %-0.161 (JA) %V/ºC
alfa_Isc = 0.0005;  % 0.0006 (JINKO)  % 0.00057 (JA)  %1/ºC
Nsc = 72;             % Cells in series
ModuleParametersJ = PVCoc(Tm,IRR,Isc,alfa_Isc,Isc0,Imp,BetaVoc,Voc,Vmp,V,I,Nsc);

%% Comparison 
Im = [ ModuleParametersJ.Isc0 ModuleParametersJ.Ix0 ModuleParametersJ.Imp0 ModuleParametersJ.Ixx0 0 ];
Vm = [ 0 ModuleParametersJ.Voc0/2 ModuleParametersJ.Vmp0 (ModuleParametersJ.Voc0+ModuleParametersJ.Vmp0)/2 ModuleParametersJ.Voc0 ];
Id = [ Isc0 Imp0 0 ]; %JA
% Id = [ 9.14 8.68 0 ]; %JINKO
Vd = [ 0 Vmp0 Voc0]; %JINKO
% Vd = [ 0 38.1 46.9 ]; %JINKO
% Cm = polyfix(Vm,Im,30,0,Isc0);
Cd = polyfix(Vd,Id,20,0,Id(1));
% xxm = linspace(0,48,500);
% yym = polyval(Cm,xxm);
xxd = linspace(0,48,500);
yyd = polyval(Cd,xxd);

% Cubic Spline data interpolation
tm = 1:numel(Vm);
xym = [Vm;Im];
ppm = spline(tm,xym);
tInterpm = linspace(1,numel(Vm));
xyInterpm = ppval(ppm, tInterpm);

% Show the result
figure(12)
plot(Vm,Im,'xr')
hold on

plot(xyInterpm(1,:),xyInterpm(2,:),'r')
hold on

plot(Vd,Id,'og');
hold on
plot(xxd,yyd,'g');
title('Comparation between points from the datasheet and the ones obtained in IV curve form')
xlabel('V (V)') 
ylabel('I (A)')
legend({'Resulted points','Interpolation','Datasheet points','Interpolation'})
ylim([0 15])

%% DC

PDClimit = 10e6; % ORURO has no limitation  %%2.6e6+2.6e6*(1-0.986);

ModuleParametersJ.Voc0 = Voc0;

ModuleParametersJ.Vmp0 = Vmp0;

ModuleParametersJ.Imp0 = Imp0;
ModuleParametersJ

Ee(isnan(Ee))=0; % Set any NaNs to zero
mSAPMResults = pvl_sapm(ModuleParametersJ, Ee, ArrayTemp);
aSAPMResults.Vmp = Ns  *mSAPMResults.Vmp;                      
aSAPMResults.Imp = Np  *mSAPMResults.Imp;
aSAPMResults.Pmp =  aSAPMResults.Vmp .* aSAPMResults.Imp;
% aSAPMResults.Pmp = aSAPMResults.Vmp .* aSAPMResults.Imp;

% LOSSES IN DC
% LOSSES FROM COMBINER BOX TO INVERTER
LengthDC2 = 4790; % Total meters of cable from combiner boxes to 1 inverter (1.1) in m
SDC2 = 300;       % Section of the cables in mm2
Icb = aSAPMResults.Imp./17;      % Current that goes through a cable from a combiner box to an inverter
T0 = 20;          % Ambient temperature of the conductor
Tmax = 90;        % Maximum admisible temperature of the conductor
Imax = 346;       % Maximum admisible current of the conductor of underearth tube
TitaAl = T0 + (Tmax-T0)*(Icb./Imax).^2;  % Temperature of the conductor
alfaAl = 0.00403;  % Resistance coefficient of aluminium
p20Al = 0.0280;    % Cable resistance at 20ºC
ptitaAl = p20Al*(1+alfaAl*(TitaAl-20));
Rcb = ptitaAl*LengthDC2/SDC2;
PlossesDC2 = Rcb.*Icb.^2;
% LOSSES FROM STRINGS OF PANELS TO COMBINER BOXES
LengthDC1 = 15*720+1316+1236; % Total meters of cable from panels to combiner boxes for 1 inverter (1.1) in m
SDC1 = 6;          % Section of the cables in mm2
Ipa = Icb./20;          % Current that goes through a cable from a STRING to a combiner box
TitaCu = 90;       % Temperature of the conductor
alfaCu = 0.00393;  % Resistance coefficient of aluminium
p20Cu = 0.017241;  % Cable resistance at 20ºC
ptitaCu = 0.02198;
Rpa = ptitaCu*LengthDC1/SDC1;
PlossesDC1 = Rpa*Ipa.^2;

PlossesDC = PlossesDC1+PlossesDC2;
PmpNoDCLosses = aSAPMResults.Pmp;
aSAPMResults.Pmp = aSAPMResults.Pmp - PlossesDC;
% aSAPMResults.Pmp = aSAPMResults.Pmp - Ploss;

figure ('OuterPosition',[100 100 600 800])
tiledlayout(3,1) % subplot(3,1,1)
% First plot
ax1 = nexttile;
plot(Time,aSAPMResults.Pmp/10^6,'-sr')
% hold on
% plot(Time,Pinj_Inv11_1/10^3,'-sg')
% hold on
% plot(Time,Pinj_Inv11_2/10^3,'-sy')
legend('Pmp')
%xlabel('Seconds of the day')
ylabel('DC Power (MW)')
title('Oruro in minutes steps, DC Power','FontSize',14)
% Second plot
ax2 = nexttile; % subplot(3,1,2)
plot(Time,aSAPMResults.Imp,'-ok')
% hold on
% plot(Time,IDC_Inv11_1,'-sg')
% hold on
% plot(Time,IDC_Inv11_2,'-sy')
legend('Imp')
ylabel('DC Current (A)')
title('Oruro in minutes steps, Current','FontSize',12)
% Third plot
ax3 = nexttile; % subplot(3,1,3)
plot(Time,aSAPMResults.Vmp,'-xk')
% hold on
% plot(Time,VDC_Inv11_1,'-sg')
% hold on
% plot(Time,VDC_Inv11_2,'-sy')
legend('Vmp')
xlabel('Minutes of the day') 
ylabel('DC Volatage (V)')
title('Oruro in minutes steps, Voltage','FontSize',12)


%% DC to AC Conversion
  Nsp = Np; % Number of parallel strings
  Ni = 1;    % Number of inverters
  PAC_InvMW = PAC_Inv11_1 * 10^3;
  PAC_InvMW2 = PAC_Inv11_2 * 10^3;
  ACPowerp = pvl_snlinverter(SMAinverter, aSAPMResults.Vmp, aSAPMResults.Pmp);   %% Importante el numero de strings en paralelo por inversor
  % At 15ºC 2750 -> 1 and at 27 ºC 2625 -> 0.9545
  % Perdida de eficiencia por temeperatura sigue la recta y = -0.0038x + 1.0570
  RedC = zeros(length(Inv1T),1);%zeros(length(AirtT_avg),1);
  ACPower = zeros(length(ACPowerp),1);
  for i=1:length(Inv1T)
        if Inv2T(i)>15
            RedC(i) = -0.0038*Inv1T(i)+1.0570;
            ACPower(i) = ACPowerp(i)*RedC(i);  
        else 
            ACPower(i) = ACPowerp(i);
        end
  end
 

% LOSSES IN AC
% TOTAL LOSSES FROM INVERTER STATIONS (after trafos) TO SUBSTATION
% There are many different paths and cables and this is just for the
% configuration of this plant. Therefore, lets assume a section of 2x300
% that is the section of the inverter station-substation cables. The length
% lets sum all the cable's lengths 
SAC2 = 2*300;
LAC2 = 875+265*7+770+345;
cosphiAC2 = 1;                     % Lets assume a unitary power factor
IAC2 = 382.6;
titaAC2 = 20+70*(IAC2/838)^2;
ptitaAC2 = p20Al*(1+alfaAl*(titaAC2-20));
RAC2 = ptitaAC2*LAC2/SAC2;
PlossesAC2 = 3*RAC2*IAC2^2;
% LOSSES FROM INVERTER TO TRAFO
nInv = 1; % Number of inverters, the added inverters 11_1 and 11_2
L1AC = 0.005; % Length of the line in km
IAC = 441; % Current of each of the 6 conductors per phase from 2646 A on nominal AC current (inverter)
R1AC = 0.265; % Inductive resistance in Ohm/km
X1AC = 0.117; % Inductive reactance in Ohm/km

PlossAC1 = nInv*3*R1AC*L1AC*IAC^2*6;
PlossesAC1per = PlossAC1/max(ACPower);

PF_Inv11_1 = PAC_Inv11_1 ./ sqrt(PAC_Inv11_1.^2+QAC_Inv11_1.^2);
PF_Inv11_2 = PAC_Inv11_2 ./ sqrt(PAC_Inv11_2.^2+QAC_Inv11_2.^2);

  ACPowerp1 = ACPower.*PF_Inv11_1;
  ACPowerp2 = ACPower.*PF_Inv11_2;
  ACPowerq1 = ACPower.*sin(acos(PF_Inv11_1));
  ACPowerq2 = ACPower.*sin(acos(PF_Inv11_2));
  
  %% active and reactive power AC
  figure(15)
  tiledlayout(2,1)
  % First plot
  ax1 = nexttile;
%   plot(Time,aSAPMResults.Pmp/10^6,'-sr')
%   hold on
  plot(Time,ACPowerp1.*Ni/10^6,'-ob')   
%   hold on
% %   plot(Time,Pinj_Inv11_1/10^3,'-sy')
% %   hold on
%   plot(Time,PAC_InvMW/10^6,'-sg')
%   legend('DC Pmp', 'AC Power 1', 'Real DC Power injected 1', 'Real AC Power injected 1')
  legend('AC active power 1')
  xlabel('Minutes')
  ylabel('Power (MW)')
  title('Oruro AC Power Output ','FontSize',14)
  % Second plot
  ax2 = nexttile;
  plot(Time,ACPowerq1.*Ni/10^6,'-ob')
%   hold on
%   plot(Time,abs(QAC_Inv11_1)/10^3,'-sg')
  linkaxes([ax1 ax2],'x')  
  legend('Reactive AC Power 1')
  xlabel('Minutes')
  ylabel('Power (MWAr)')
  title('Oruro AC Reactive Power Output ','FontSize',14) 
  
  figure(16)
  tiledlayout(2,1)
  % First plot
  ax1 = nexttile;
%   plot(Time,aSAPMResults.Pmp/10^6,'-sr')
%   hold on
  plot(Time,ACPowerp2.*Ni/10^6,'-ob')   
%   hold on
% %   plot(Time,Pinj_Inv11_2/10^3,'-sy')
% %   hold on
%   plot(Time,PAC_InvMW2/10^6,'-sg')
%   legend('DC Pmp', 'AC Power 2', 'Real DC Power injected 2', 'Real AC Power injected 2')
  legend('AC active power 2')
  xlabel('Minutes')
  ylabel('Power (MW)')
  title('AC Power Output ','FontSize',14)
  % Second plot
  ax2 = nexttile;
  plot(Time,ACPowerq2.*Ni/10^6,'-ob')
%   hold on
%   plot(Time,abs(QAC_Inv11_2)/10^3,'-sg')
  linkaxes([ax1 ax2],'x')  
  legend('Reactive AC Power 2')
  xlabel('Minutes')
  ylabel('Power (MWAr)')
  title('Oruro AC Reactive Power Output ','FontSize',14) 
  
  %% power factor
  figure(17)
  tiledlayout(3,1) % subplot(3,1,1)
% First plot
ax1 = nexttile;
plot(Time,PFE_Inv11_1);
hold on
plot(Time,PF_Inv11_1);
legend('PF excel', 'PF calculated')
 xlabel('Time in minutes')
 ylabel('cos(phi)')
 title('Inverter 11_1 ','FontSize',14)
 ylim([-1.5 1.5])
% Second plot
ax2 = nexttile; % subplot(3,1,2)
plot(Time,PFE_Inv11_2);
hold on
plot(Time,PF_Inv11_2);
legend('PF excel', 'PF calculated')
 xlabel('Time in minutes')
 ylabel('cos(phi)')
 title('Inverter 11_2 ','FontSize',14)
 ylim([-1.5 1.5])
% Third plot
ax3 = nexttile;
plot(Time,[PF_Inv11_1,PF_Inv11_2])
ylim([0 1.5])
linkaxes([ax1 ax2 ax3],'x')

%% PEAK AC VOLTAGES AND DC VOLTAGES
  figure(18)
  tiledlayout(2,1) % subplot(3,1,1)
% First plot
ax1 = nexttile;
plot(Time,aSAPMResults.Vmp,'-xk')
hold on
plot(Time,sqrt(2)*VAC_Inv11_1_LL12,'-m');
legend('DC Voltage calculated', 'Peak AC VOLAGE L1-2 DATA')
 xlabel('Time in minutes')
 ylabel('Voltage (V)')
 title('Inverter 11_1 ','FontSize',14)
% Second plot
ax2 = nexttile; % subplot(3,1,2)
plot(Time,sqrt(2)*VAC_Inv11_1_LL12./aSAPMResults.Vmp);
legend('Peak AC L1-2 V divided by DC Voltage')
 xlabel('Time in minutes')
 ylabel('Voltage (V)')
 title('Inverter 11_1 ','FontSize',14)
linkaxes([ax1 ax2],'x')
 
  figure(19)
  tiledlayout(3,1) % subplot(3,1,1)
% First plot
ax1 = nexttile;
plot(Time,aSAPMResults.Vmp,'-xk')
hold on
plot(Time,sqrt(2)*VAC_Inv11_2_LL12,'-m');
legend('DC Voltage calculated', 'Peak AC VOLAGE L1-2 DATA')
 xlabel('Time in minutes')
 ylabel('Voltage (V)')
 title('Inverter 11_2 ','FontSize',14)
% Second plot
ax2 = nexttile; % subplot(3,1,2)
plot(Time,sqrt(2)*VAC_Inv11_2_LL12./aSAPMResults.Vmp);
legend('Peak AC L1-2 V divided by DC Voltage')
 xlabel('Time in minutes')
 ylabel('Voltage (V)')
 title('Inverter 11_2 ','FontSize',14)
% Third plot
ax3 = nexttile; 
plot(Time,[sqrt(2)*VAC_Inv11_2_LL12./aSAPMResults.Vmp,sqrt(2)*VAC_Inv11_1_LL12./aSAPMResults.Vmp])
legend('Peak AC L1-2 V divided by DC Voltage','Peak AC L1-2 V divided by DC Voltage')
linkaxes([ax1 ax2 ax3],'x')


%% Efficiencies  
  PAC_PVsyst = PAC_InvMW;
  ACPowerInv = ACPower.*Ni;
  effAC = zeros(length(ACPowerInv),1);
  AvgEffAC = 0;
  for i = 1:length(ACPowerInv)
     if PAC_PVsyst(i) < ACPowerInv(i)
      effAC(i) = PAC_PVsyst(i)/ACPowerInv(i)*100;
     else 
      effAC(i) = ACPowerInv(i)/PAC_PVsyst(i)*100;
     end
  end
 effAC(isnan(effAC)) = 100; % Set any NaNs to 100
 for i = 1:length(effAC)
    AvgEffAC = AvgEffAC + effAC(i); 
 end
  AvgEffAC = AvgEffAC / length(effAC);
% plot(Minutes,Pinj_Inv5_2MW./ACPower.*Ni)


% PERFOMANCE CALCULATION
%% PR


    
       ACPowerPRd = sum(ACPower);
       Irr1PRd = sum(POA_avg);

Yf = (ACPowerPRd.*Ni)./(330*24*330*Ni);
Yr = Irr1PRd/1000;
PR = Yf./Yr;

