%% REAL DATA
% ESTUDIO2019DICIEMBRE
% ESTUDIO2020JUNIO
% Inversor 5_2
[num1,Time1,both1] = xlsread('ESTUDIO_2019_DICIEMBRE.xls','A2:A44160');
[num2,Time2,both2] = xlsread('ESTUDIO_2020_JUNIO.xls','A2:A43154');
Time = [Time1; Time2];
MES1 = xlsread('ESTUDIO_2019_DICIEMBRE.xls');
MES2 = xlsread('ESTUDIO_2020_JUNIO.xls');
Pinj_Inv5_2D = xlsread('ESTUDIO_2019_DICIEMBRE.xls','J2:J44160');
Pinj_Inv5_2J = xlsread('ESTUDIO_2020_JUNIO.xls','K2:K43154');

lengthD = size(MES1);
lengthJ = size(MES2);
AirT_1 = zeros(lengthD(1)+lengthJ(1),1);
POA_1 = zeros(lengthD(1)+lengthJ(1),1);
SURFACET_1_1 = zeros(lengthD(1)+lengthJ(1),1);
SURFACET_1_2 = zeros(lengthD(1)+lengthJ(1),1);
AirT_2 = zeros(lengthD(1)+lengthJ(1),1);
POA_2 = zeros(lengthD(1)+lengthJ(1),1);
SURFACET_2_1 = zeros(lengthD(1)+lengthJ(1),1);
SURFACET_2_2 = zeros(lengthD(1)+lengthJ(1),1);
Pinj = zeros(lengthD(1)+lengthJ(1),1);

Pinj_Inv5_2 = [Pinj_Inv5_2D; Pinj_Inv5_2J];

 for i = 20:lengthD(2)
     matrix = [MES1(:,i);MES2(:,i)];
     vec = genvarname(['vec',num2str(i)]);
     eval([vec, '=matrix']);   
 end

%% EXTRACTED VARIABLES
AirT_1  = vec20;
POA_1 = vec21;
SURFACET_1_1 = vec22;
SURFACET_1_2 = vec23;
AirT_2 = vec24;
POA_2 = vec25;
SURFACET_2_1 = vec26;
SURFACET_2_2 = vec27;
Pinj = vec28;
POA_avg = (POA_1 + POA_2)/2;
AirtT_avg = (AirT_1+AirT_2)/2;
SURFACET_1_avg = (SURFACET_1_1 + SURFACET_1_2)/2;
SURFACET_2_avg = (SURFACET_2_1 + SURFACET_2_2)/2;
SURFACET_avg = (SURFACET_1_avg + SURFACET_2_avg)/2;

%% INJECTED POWER PLOT
Minutes = 1:size(AirT_1);
Time = datetime(Time,"Format","dd-MMM-yyyy HH:mm:ss");
plot(Time,Pinj_Inv5_2*10^3) % In the excel the data is in KW