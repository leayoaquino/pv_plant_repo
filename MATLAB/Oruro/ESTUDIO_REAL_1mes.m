%% REAL DATA
% 1 MES
% ESTUDIO2020JUNIO
% Inversor 5_2 y unas horas solo

[num2,Time2,both2] = xlsread('ESTUDIO_2020_JUNIO.xls','A4682:A5522');%xlsread('ESTUDIO_2020_JUNIO.xls','A2:A43154');
Time = Time2;

MES = xlsread('ESTUDIO_2020_JUNIO.xls','4682:5522'); %xlsread('ESTUDIO_2020_JUNIO.xls')
InvT = xlsread('Oruro F2 04_05.xlsx','AD3:AD843');
Pinj_Inv5_2 = xlsread('ESTUDIO_2020_JUNIO.xls','K4682:K5522');%xlsread('ESTUDIO_2020_JUNIO.xls','K2:K43154');

length1 = size(MES);

AirT_1 = zeros(length1(1),1);
POA_1 = zeros(length1(1),1);
SURFACET_1_1 = zeros(length1(1),1);
SURFACET_1_2 = zeros(length1(1),1);
AirT_2 = zeros(length1(1),1);
POA_2 = zeros(length1(1),1);
SURFACET_2_1 = zeros(length1(1),1);
SURFACET_2_2 = zeros(length1(1),1);
Pinj = zeros(length1(1),1);



 for i = 20:length1(2)
     matrix = MES(:,i);
     vec = genvarname(['vec',num2str(i)]);
     eval([vec, '=matrix']);   
 end

%% EXTRACTED VARIABLES
AirT_1  = vec20;
POA_1 = vec21;
SURFACET_1_1 = vec22;
SURFACET_1_2 = vec23;
AirT_2 = vec24;
POA_2 = vec25;
SURFACET_2_1 = vec26;
SURFACET_2_2 = vec27;
Pinj = vec28;
POA_avg = (POA_1 + POA_2)/2;
AirtT_avg = (AirT_1+AirT_2)/2;
SURFACET_1_avg = (SURFACET_1_1 + SURFACET_1_2)/2;
SURFACET_2_avg = (SURFACET_2_1 + SURFACET_2_2)/2;
SURFACET_avg = (SURFACET_1_avg + SURFACET_2_avg)/2;

%% INJECTED POWER PLOT
Minutes = 1:size(AirT_1);
Time = datetime(Time,"Format","dd-MMM-yyyy HH:mm:ss");
plot(Time,Pinj_Inv5_2*10^3) % In the excel the data is in KW