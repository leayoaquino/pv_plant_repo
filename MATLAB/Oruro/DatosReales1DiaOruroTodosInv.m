%% REAL DATA
% 1 DÍA
% MAYO 2021
% Inversor 11_1 Y 11_2 y unas horas solo

Time = table2array(OruroF20405inv(:,1));
Time = datetime(Time, 'InputFormat', 'dd/MM/yyyy HH:mm:ss');

InvDCcurrents = zeros(size(OruroF20405inv,1),20);
InvDCvoltages = zeros(size(OruroF20405inv,1),20);
InvDCQs = zeros(size(OruroF20405inv,1),20);
InvDCPs = zeros(size(OruroF20405inv,1),20);
InvDCQsSP = zeros(size(OruroF20405inv,1),20);
j = 1;
for i = 2:5:size(OruroF20405inv,2)-1
   InvDCcurrents(:,j) = table2array(OruroF20405inv(:,i));
   InvDCvoltages(:,j) = table2array(OruroF20405inv(:,i+1));
   InvDCQs(:,j) = table2array(OruroF20405inv(:,i+2)); 
   InvDCPs(:,j) = table2array(OruroF20405inv(:,i+3));
   InvDCQsSP(:,j) = table2array(OruroF20405inv(:,i+4));
   j = j+1;
end

figure(1)
for i = 1:size(InvDCcurrents,2)
   plot(Time,InvDCcurrents(:,i));
   hold on
end
legend('Inv11_1', 'Inv11_2','Inv12_1', 'Inv12_2','Inv13_1', 'Inv13_2','Inv14_1', 'Inv14_2','Inv15_1', 'Inv15_2','Inv16_1', 'Inv16_2','Inv17_1', 'Inv17_2','Inv18_1', 'Inv18_2','Inv19_1', 'Inv19_2','Inv20_1', 'Inv20_2')
xlabel('Time in minutes')
ylabel('DC currents')
title('DC Currents from each inverter','FontSize',14)
figure(2)
for i = 1:size(InvDCvoltages,2)
   plot(Time,InvDCvoltages(:,i));
   hold on
end
legend('Inv11_1', 'Inv11_2','Inv12_1', 'Inv12_2','Inv13_1', 'Inv13_2','Inv14_1', 'Inv14_2','Inv15_1', 'Inv15_2','Inv16_1', 'Inv16_2','Inv17_1', 'Inv17_2','Inv18_1', 'Inv18_2','Inv19_1', 'Inv19_2','Inv20_1', 'Inv20_2')
xlabel('Time in minutes')
ylabel('DC Voltages')
title('DC Voltages from each inverter','FontSize',14)
figure(3)
for i = 1:size(InvDCQs,2)
   plot(Time,InvDCQs(:,i));
   hold on
end
legend('Inv11_1', 'Inv11_2','Inv12_1', 'Inv12_2','Inv13_1', 'Inv13_2','Inv14_1', 'Inv14_2','Inv15_1', 'Inv15_2','Inv16_1', 'Inv16_2','Inv17_1', 'Inv17_2','Inv18_1', 'Inv18_2','Inv19_1', 'Inv19_2','Inv20_1', 'Inv20_2')
xlabel('Time in minutes')
ylabel('Reactive Power total')
title('Total reactive power from each inverter','FontSize',14)
figure(4)
for i = 1:size(InvDCPs,2)
   plot(Time,InvDCPs(:,i));
   hold on
end
legend('Inv11_1', 'Inv11_2','Inv12_1', 'Inv12_2','Inv13_1', 'Inv13_2','Inv14_1', 'Inv14_2','Inv15_1', 'Inv15_2','Inv16_1', 'Inv16_2','Inv17_1', 'Inv17_2','Inv18_1', 'Inv18_2','Inv19_1', 'Inv19_2','Inv20_1', 'Inv20_2')
xlabel('Time in minutes')
ylabel('Active Power total')
title('Total active power from each inverter','FontSize',14)
figure(5)
for i = 1:size(InvDCQsSP,2)
   plot(Time,InvDCQsSP(:,i));
   hold on
end
legend('Inv11_1', 'Inv11_2','Inv12_1', 'Inv12_2','Inv13_1', 'Inv13_2','Inv14_1', 'Inv14_2','Inv15_1', 'Inv15_2','Inv16_1', 'Inv16_2','Inv17_1', 'Inv17_2','Inv18_1', 'Inv18_2','Inv19_1', 'Inv19_2','Inv20_1', 'Inv20_2')
xlabel('Time in minutes')
ylabel('Reactive Power set point')
title('Seat point reactive power from each inverter','FontSize',14)