% SI QUEREMOS COMPARAR PRIMERO RUNEAR EL SCRIPT DE DATOS REALES
% También necesario para los datos de temperatura e irradiancia
%% PV MODULE
% First of all we need to know the module we are going to use or try

load('CECModuleSAM.mat')       % Huge datasheet with modules with CEC parameters
% JKM330PP-72-V  # 10714
% Module = CECModuleSAM(10714);
% If we don't have the desired moudule in the datasheet we should introduce
% its characteristics
% Ind this case we have it, these values are the same as in Module = CECModuleSAM(10714);
Module.name = 'Jinko Solar JKM320PP-72';
Module.a_ref = 1.87478;
Module.IL_ref = 9.23276;
Module.I0_ref = 1.2592e-10;
Module.Rsh_ref = 2754.66;
Module.Rs_ref = 0.406344;
Module.alpha_sc = 0.005694;
Module.adjust = 9.24109;
% We need to include also the efficiency that appears on the module
% datasheet
Module.effic = 17.01; % in %

%% Predicting IV curve points

% We call the function pvl_calcparams_CECmio to calculate the desired IV
% curves and data for different T and E in the module. 
% Results are saved in IVResult
[IL, I0, Rs, Rsh, S, Tcell, Tm, a] = pvl_calcparams_CECmio(Module);
NumPoints = 1000;
[IVResult] = pvl_singlediode(IL, I0, Rs, Rsh, a, NumPoints);

% Plots of the curves
figure
for i=1:length(S)
    plot(IVResult.V(i,:),IVResult.I(i,:))
    hold on
    scatter(IVResult.Vmp(i),IVResult.Imp(i),'filled')
    text(2,IVResult.Isc(i)+0.3,[num2str(S(i)) ' W/m^2 at ' num2str(Tcell(i)) ' ºC'])
end
xlabel('Voltage (V)')
ylabel('Current (A)')
title('IV Curve from CEC Single Diode Model','FontSize',14)
ylim([0 14])


%% NOW WE ARE GOING TO START WITH THE EMULATING OF THE POWER

%% Transfer the obtained data 
Tm = Tcell';
IRR = S';
Isc = IVResult.Isc;
Imp = IVResult.Imp;
Voc = IVResult.Voc;
Vmp = IVResult.Vmp;
V = IVResult.V';
I = IVResult.I';

%% Inverter
% Different databases for the inverter
load('SandiaInverterDatabaseSAM2014.1.14.mat')
load('CECInverterDatabaseSAM2015.6.30.mat')
load('DriesseInverterDatabaseSAM2013.10.mat')
% If we don't want to use an inverter from the database we can introduce
% its parameters. For C coefficients we can use SAM
SMAinverter.name = 'Ingecon Sun 1640TL U B630';   %SAM PARAMETERS
SMAinverter.vac = 630;
SMAinverter.Pac0 = 1.456e+06;  % 21.45/16
SMAinverter.Pdc0 = 1.456e+06 + 1.456e+06 *(1-0.986);
SMAinverter.Vdc0 = 1060;
SMAinverter.Ps0 = 4150.83;
SMAinverter.C0 = -1.0885e-08;
SMAinverter.C1 = 1e-05;
SMAinverter.C2 = 0.000499;
SMAinverter.C3 = 9.5e-05;
SMAinverter.Pnt = 410.1228;


%% Define array configuration
Ns = 30;   % Number of modules in series
Np = 20*9; % Number of parallel strings

%% Irradiance and Temperature for a year
Ee = POA/1000;
ArrayTemp = MODULET;       

% prompt = 'Type the Isc0 of the PV module: ';
% Isc0 = input(prompt);
Isc0 = 9.1;
% prompt = 'Type the Voc0 of the PV module: ';
% Voc0 = input(prompt);
Voc0 = 46.9;
% prompt = 'Type the Imp0 of the PV module: ';
% Imp0 = input(prompt);
Imp0 = 8.7;
% prompt = 'Type the Vmp0 of the PV module: ';
% Vmp0 = input(prompt);
Vmp0 = 38;
BetaVoc =  -0.143;   %-0.144; (JINKO x SAM) %-0.161 (JA) %V/ºC
alfa_Isc = 0.0006;  % 0.0006 (JINKO)  % 0.00057 (JA)  %1/ºC
Nsc = 72;             % Cells in series
ModuleParametersJ = PVCoc(Tm,IRR,Isc,alfa_Isc,Isc0,Imp,BetaVoc,Voc,Vmp,V,I,Nsc);

%% Comparison
Im = [ ModuleParametersJ.Isc0 ModuleParametersJ.Ix0 ModuleParametersJ.Imp0 ModuleParametersJ.Ixx0 0 ];
Vm = [ 0 ModuleParametersJ.Voc0/2 ModuleParametersJ.Vmp0 (ModuleParametersJ.Voc0+ModuleParametersJ.Vmp0)/2 ModuleParametersJ.Voc0 ];
Id = [ Isc0 Imp0 0 ]; %JA
% Id = [ 9.14 8.68 0 ]; %JINKO
Vd = [ 0 Vmp0 Voc0]; %JINKO
% Vd = [ 0 38.1 46.9 ]; %JINKO
% Cm = polyfix(Vm,Im,30,0,Isc0);
Cd = polyfix(Vd,Id,20,0,Id(1));
% xxm = linspace(0,48,500);
% yym = polyval(Cm,xxm);
xxd = linspace(0,48,500);
yyd = polyval(Cd,xxd);

% Cubic Spline data interpolation
tm = 1:numel(Vm);
xym = [Vm;Im];
ppm = spline(tm,xym);
tInterpm = linspace(1,numel(Vm));
xyInterpm = ppval(ppm, tInterpm);

% Show the result
figure(12)
plot(Vm,Im,'xr')
hold on

plot(xyInterpm(1,:),xyInterpm(2,:),'r')
hold on

plot(Vd,Id,'og');
hold on
plot(xxd,yyd,'g');
title('Comparation between points from the datasheet and the ones obtained in IV curve form')
xlabel('V (V)') 
ylabel('I (A)')
legend({'Resulted points','Interpolation','Datasheet points','Interpolation'})
ylim([0 15])

%% DC

PDClimit = 1.456e+06 + 1.456e+06 *(1-0.986);

ModuleParametersJ.Voc0 = Voc0;

ModuleParametersJ.Vmp0 = Vmp0;

ModuleParametersJ.Imp0 = Imp0;

Ee(isnan(Ee))=0; % Set any NaNs to zero
mSAPMResults = pvl_sapm(ModuleParametersJ, Ee, ArrayTemp);
aSAPMResults.Vmp = Ns  *mSAPMResults.Vmp;                      
aSAPMResults.Imp = Np  *mSAPMResults.Imp;

aSAPMResults.Pmp = zeros(size(aSAPMResults.Vmp,1),1);
% Power limitation
for i = 1:size(aSAPMResults.Vmp,1)
if (aSAPMResults.Vmp(i)*aSAPMResults.Imp(i))>PDClimit   % If the actual power is above the limit for each inverter
    aSAPMResults.Imp(i) = PDClimit/aSAPMResults.Vmp(i); % Calculate the new DC current with the limit P and unvariable V
    aSAPMResults.Pmp(i) = aSAPMResults.Vmp(i) .* aSAPMResults.Imp(i);
else 
    aSAPMResults.Imp(i) = Np  *mSAPMResults.Imp(i);     % If the actual power is below the limit for each inverter, keep it
    aSAPMResults.Pmp(i) = aSAPMResults.Vmp(i) .* aSAPMResults.Imp(i);
end
end

% DC power losses, assume a 1%, it should be introduced by yourself
Ploss = 0.01;
aSAPMResults.Pmp = aSAPMResults.Pmp - aSAPMResults.Pmp.*Ploss;


figure('OuterPosition',[100 100 600 800])
tiledlayout(3,1) % subplot(3,1,1)
% First plot
ax1 = nexttile;
plot(TimeVer,aSAPMResults.Pmp/10^6,'-sr')
hold on
plot(TimeVer,PDC_Inv1_2/10^3,'-sg')
legend('Pmp','Real data DC power')
%xlabel('Seconds of the day')
ylabel('DC Power (MW)')
title('Versalles, DC Power','FontSize',14)
% Second plot
ax2 = nexttile; % subplot(3,1,2)
plot(TimeVer,aSAPMResults.Imp,'-ok')
hold on
plot(TimeVer,IDC_Inv1_2,'-sg')
legend('Imp','Excel data DC current')
%xlabel('Seconds of the day')
ylabel('DC Current (A)')
title('Versalles, Current','FontSize',12)
% Third plot
ax3 = nexttile; % subplot(3,1,3)
plot(TimeVer,aSAPMResults.Vmp,'-xk')
hold on
plot(TimeVer,VDC_Inv1_2,'-sg')
legend('Vmp','Excel data DC voltage')
xlabel('Minutes') 
ylabel('DC Volatage (V)')
title('Versalles, Voltage','FontSize',12)

linkaxes([ax1 ax2 ax3],'x')

%% DC to AC Conversion
Nsp = Np; % Number of parallel strings
Ni = 1;    % Number of inverters
ACPower = pvl_snlinverter(SMAinverter, aSAPMResults.Vmp, aSAPMResults.Pmp);   

% DERATING DEL INVERSOR SEGÚN LA CURVA PROPORCIONADA POR SMA PARA ORURO  
  % At 15ºC 2750 -> 1 and at 27 ºC 2625 -> 0.9545
  % Perdida de eficiencia por temeperatura sigue la recta y = -0.0038x + 1.0570
%   RedC = zeros(length(Inv1T),1);%zeros(length(AirtT_avg),1);
%   ACPower = zeros(length(ACPowerp),1);
%   for i=1:length(Inv1T)
%         if Inv2T(i)>15
%             RedC(i) = -0.0038*Inv1T(i)+1.0570;
%             ACPower(i) = ACPowerp(i)*RedC(i);  
%         else 
%             ACPower(i) = ACPowerp(i);
%         end
%   end

PF_Inv1_2 = PAC_Inv1_2./sqrt(PAC_Inv1_2.^2+QAC_Inv1_2.^2);
PF_Inv1_2(isnan(PF_Inv1_2))=0; % Set any NaNs to zero
ACPowerp = ACPower.*PF_Inv1_2;
ACPowerq = ACPower.*sin(acos(PF_Inv1_2));

% Apply a 0.5% of losses in AC wiring 
ACPower = ACPower - ACPower*0.005;

  %% active and reactive power AC
  figure(15)
  tiledlayout(2,1)
  % First plot
  ax1 = nexttile;
  plot(TimeVer,ACPowerp.*Ni/10^6,'-ob')   
%   hold on
%   plot(TimeVer,PAC_Inv1_2/10^3,'-sg')
  legend('Real AC power')
  xlabel('Minutes')
  ylabel('Power (MW)')
  title('Versalles AC Power Output ','FontSize',14)
  % Second plot
  ax2 = nexttile;
  plot(TimeVer,ACPowerq.*Ni/10^6,'-ob')
%   hold on
%   plot(TimeVer,QAC_Inv1_2/10^3,'-sg')
  linkaxes([ax1 ax2],'x')  
  legend('Reactive AC power')
  xlabel('Minutes')
  ylabel('Power (MWAr)')
  title('Versalles AC Reactive Power Output ','FontSize',14) 