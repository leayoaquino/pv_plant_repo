%% REAL DATA
% Inversores
% Estaciones meteorológicas
% Inversor 1.2
[num1,Time1,both1] = xlsread('InversoresVersalles.xlsx','A87902:A131101');
[num2,Time2,both2] = xlsread('InversoresVersalles.xlsx','A220382:A260701');
Time = [Time1; Time2];

Pinj_Inv2_1 = xlsread('InversoresVersalles.xlsx','X87902:X131101');
Qinj_Inv2_1 = xlsread('InversoresVersalles.xlsx','Y87902:Y131101');
VDC_Inv2_1 = xlsread('InversoresVersalles.xlsx','O87902:O131101');
IDC_Inv2_1 = xlsread('InversoresVersalles.xlsx','P87902:P131101');
Pinj_Inv2_2 = xlsread('InversoresVersalles.xlsx','X220382:X260701');
Qinj_Inv2_2 = xlsread('InversoresVersalles.xlsx','Y220382:Y260701');
VDC_Inv2_2 = xlsread('InversoresVersalles.xlsx','O220382:O260701');
IDC_Inv2_2 = xlsread('InversoresVersalles.xlsx','P220382:P260701');

POA_1 = xlsread('Estaciones meteorológicas.xlsx','D87902:D131101');
POA_2 = xlsread('Estaciones meteorológicas.xlsx','D220382:D260701');
MODULET_1_1 = xlsread('Estaciones meteorológicas.xlsx','G87902:G131101');
MODULET_1_2 = xlsread('Estaciones meteorológicas.xlsx','H87902:H131101');
MODULET_1_3 = xlsread('Estaciones meteorológicas.xlsx','I87902:I131101');
MODULET_2_1 = xlsread('Estaciones meteorológicas.xlsx','G220382:G260701');
MODULET_2_2 = xlsread('Estaciones meteorológicas.xlsx','H220382:H260701');
MODULET_2_3 = xlsread('Estaciones meteorológicas.xlsx','I220382:I260701');

POA = [POA_1;POA_2];
IDC_Inv2 = [IDC_Inv2_1;IDC_Inv2_2];
VDC_Inv2 = [VDC_Inv2_1;VDC_Inv2_2];
Pinj_Inv2 = [Pinj_Inv2_1;Pinj_Inv2_2];
Qinj_Inv2 = [Qinj_Inv2_1;Qinj_Inv2_2];
PDC_Inv2 = [VDC_Inv2_1.*IDC_Inv2_1;VDC_Inv2_2.*IDC_Inv2_2];
MODULET_1 = (MODULET_1_1 + MODULET_1_2 + MODULET_1_3)/3;
MODULET_2 = (MODULET_2_1 + MODULET_2_2 + MODULET_2_3)/3;
MODULET = [MODULET_1;MODULET_2];


%% INJECTED POWER PLOT
Minutes = 1:size(Pinj_Inv2);
Time = datetime(Time,"InputFormat","dd/MM/yyyy HH:mm:ss");
plot(Time,Pinj_Inv2*10^3) % In the excel the data is in KW
hold on
plot(Time,PDC_Inv2)