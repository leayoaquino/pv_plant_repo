%% REAL DATA
% Inversores
% Estaciones meteorológicas
TimeVer =  Inversores{:,1};

%% INVERSOR 1.2

PDC_Inv1_2 = Inversores{:,17};
VDC_Inv1_2 = Inversores{:,15};
IDC_Inv1_2 = Inversores{:,16};
PAC_Inv1_2 = Inversores{:,24};
QAC_Inv1_2 = Inversores{:,25};

%% Analizador de planta

Pplant = Analizadordeplanta{:,2};
Qplant = Analizadordeplanta{:,3};
PFplant = Analizadordeplanta{:,4}; 

%% Estación meteorologia

POA_1 = Estacionesmeteorolgicas{:,3};
POA_2 = Estacionesmeteorolgicas{:,4};
MODULET_1 = Estacionesmeteorolgicas{:,7};
MODULET_2 = Estacionesmeteorolgicas{:,8};
MODULET_3 = Estacionesmeteorolgicas{:,9};
Windspeed = Estacionesmeteorolgicas{:,5};
Tamb = Estacionesmeteorolgicas{:,6};

POA = (POA_1 + POA_2)/2;
MODULET = (MODULET_1 + MODULET_2 + MODULET_3)/3;
CELLT = MODULET + (POA/1000)*3;

%% Cell temp calculation
a1 = -3.47;
b1 = -0.0594;
IncTcnd = 3;
a2 = -3.56;
b2 = -0.0750;
a3 = -3.58;
b3 = -0.1130;
Tm = POA.*(exp(a2+b2*Windspeed)) + Tamb;
Tcell = Tm + (POA/1000)*IncTcnd;

figure(1)
plot(TimeVer,CELLT)
hold on
plot(TimeVer,Tcell)
legend('Tcell', 'Tcell calculated')


%% INJECTED POWER PLOT
% Minutes = 1:size(Pinj_Inv2);
% Time = datetime(Time,"InputFormat","dd/MM/yyyy HH:mm:ss");
% plot(Time,Pinj_Inv2*10^3) % In the excel the data is in KW
% hold on
% plot(Time,PDC_Inv2)