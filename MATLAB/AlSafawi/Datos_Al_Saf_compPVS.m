
TimeAl1 =  SAFAWIFRV50MWJORDANVCTHourlyRes0{:,1};
TimeAl1 = datetime(TimeAl1,'InputFormat', 'dd/MM/yyyy HH:mm');

% Meteo
 
SurfT_1 = SAFAWIFRV50MWJORDANVCTHourlyRes0{:,3}; 

Irr1 = SAFAWIFRV50MWJORDANVCTHourlyRes0{:,2}; 

% Tension y corriente en DC 
I_DC = SAFAWIFRV50MWJORDANVCTHourlyRes0{:,5};
V_DC = SAFAWIFRV50MWJORDANVCTHourlyRes0{:,6};
P_DC = SAFAWIFRV50MWJORDANVCTHourlyRes0{:,4};


% 
P_AC = SAFAWIFRV50MWJORDANVCTHourlyRes0{:,7};
S_AC = SAFAWIFRV50MWJORDANVCTHourlyRes0{:,8};
Q_AC = SAFAWIFRV50MWJORDANVCTHourlyRes0{:,9};