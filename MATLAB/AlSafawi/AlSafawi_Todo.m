% SI QUEREMOS COMPARAR PRIMERO RUNEAR EL SCRIPT DE DATOS REALES
% También necesario para los datos de temperatura e irradiancia
%% PV MODULE
% First of all we need to know the module we are going to use or try

load('CECModuleDatabaseSAM2015.6.30.mat')       % Huge datasheet with modules with CEC parameters
% JA Solar JAP6-72-330/3BB  
% Module = CECModuleDB(5623);
% If we don't have the desired moudule in the datasheet we should introduce
% its characteristics
Module.name = 'JA  Solar JAP72D00-330/SC';
Module.a_ref = 1.851873;
Module.IL_ref = 9.185898;
Module.I0_ref = 1.44297e-10;
Module.Rsh_ref = 498.759167;
Module.Rs_ref = 0.271536;
Module.alpha_sc = 0.004545;  % A/C
Module.adjust = 9.234706;
Module.effic = 16.90; % in (%)
% The efficiency of the PV module should be given in the data sheet

%% Predicting IV curve points


% We call the function pvl_calcparams_CEC to calculate the desired IV
% curves and data for different T and E in the module. 
% Results are saved in IVResult
[IL, I0, Rs, Rsh, S, Tcell, Tm, a] = pvl_calcparams_CECmio(Module);
NumPoints = 1000;
[IVResult] = pvl_singlediode(IL, I0, Rs, Rsh, a, NumPoints);

% Plots of the curves
figure
for i=1:length(S)
    plot(IVResult.V(i,:),IVResult.I(i,:))
    hold on
    scatter(IVResult.Vmp(i),IVResult.Imp(i),'filled')
    text(2,IVResult.Isc(i)+0.3,[num2str(S(i)) ' W/m^2 at ' num2str(Tcell(i)) ' ºC'])
end
xlabel('Voltage (V)')
ylabel('Current (A)')
title('IV Curve from CEC Single Diode Model','FontSize',14)
ylim([0 14])

%% NOW WE ARE GOING TO START WITH THE EMULATING OF THE POWER

%% Transfer the obtained data 
Tm = Tcell';
IRR = S';
Isc = IVResult.Isc;
Imp = IVResult.Imp;
Voc = IVResult.Voc;
Vmp = IVResult.Vmp;
V = IVResult.V';
I = IVResult.I';

%% Inverter
% Different databases for the inverter
load('SandiaInverterDatabaseSAM2014.1.14.mat')
load('CECInverterDatabaseSAM2015.6.30.mat')
load('DriesseInverterDatabaseSAM2013.10.mat')
% If we don't want to use an inverter from the database we can introduce
% its parameters. For C coefficients we can use SAM
SMAinverter.name = 'Power Electronics FS2800CH15_645V';   %SAM PARAMETERS
SMAinverter.vac = 645;
SMAinverter.Pac0 = 2.6e6; %330*30*307*0.986; %2.36e6*0.986; 
SMAinverter.Pdc0 =  2.6e6 + 2.6e6*(1-0.986); %330*30*307; %2.36e6;
SMAinverter.Vdc0 = 957;
SMAinverter.Ps0 = 11198.9;
SMAinverter.C0 = -4.03041e-09;
SMAinverter.C1 = 2.37481e-06;
SMAinverter.C2 = 5.7e-05;
SMAinverter.C3 = -2.6e-05;
SMAinverter.Pnt = 907.473;
% SMAinverter.Vdcmax = 1200;
% SMAinverter.Idcmax = 2797.284;
% SMAinverter.MPPTLow = 875;
% SMAinverter.MPPTHi = 1200;
% SMAinverter.LibraryType = {'Sandia Inverter'};
% SMAinverter.LibraryName = {'Sandia Inverters'};

%% Define array configuration
Ns = 30;   % Number of modules in series
Np = 306; % Number of parallel strings

%% Irradiance and Temperature for a year
Ee = Irr1/1000;
ArrayTemp = (SurfT_1+SurfT_1)/2;       % Surface Temperature
% prompt = 'Type the Isc0 of the PV module: ';
% Isc0 = input(prompt);
Isc0 = 9.11;
% prompt = 'Type the Voc0 of the PV module: ';
% Voc0 = input(prompt);
Voc0 = 46.36;
% prompt = 'Type the Imp0 of the PV module: ';
% Imp0 = input(prompt);
Imp0 = 8.61;
% prompt = 'Type the Vmp0 of the PV module: ';
% Vmp0 = input(prompt);
Vmp0 = 38.33;
BetaVoc =  -0.143;   %-0.144; (JINKO x SAM) %-0.161 (JA) %V/ºC
alfa_Isc = 0.0005;  % 0.0006 (JINKO)  % 0.00057 (JA)  %1/ºC
Nsc = 72;             % Cells in series
ModuleParametersJ = PVCoc(Tm,IRR,Isc,alfa_Isc,Isc0,Imp,BetaVoc,Voc,Vmp,V,I,Nsc);

%% Comparison
Im = [ ModuleParametersJ.Isc0 ModuleParametersJ.Ix0 ModuleParametersJ.Imp0 ModuleParametersJ.Ixx0 0 ];
Vm = [ 0 ModuleParametersJ.Voc0/2 ModuleParametersJ.Vmp0 (ModuleParametersJ.Voc0+ModuleParametersJ.Vmp0)/2 ModuleParametersJ.Voc0 ];
Id = [ Isc0 Imp0 0 ]; %JA
% Id = [ 9.14 8.68 0 ]; %JINKO
Vd = [ 0 Vmp0 Voc0]; %JINKO
% Vd = [ 0 38.1 46.9 ]; %JINKO
% Cm = polyfix(Vm,Im,30,0,Isc0);
Cd = polyfix(Vd,Id,20,0,Id(1));
% xxm = linspace(0,48,500);
% yym = polyval(Cm,xxm);
xxd = linspace(0,48,500);
yyd = polyval(Cd,xxd);

% Cubic Spline data interpolation
tm = 1:numel(Vm);
xym = [Vm;Im];
ppm = spline(tm,xym);
tInterpm = linspace(1,numel(Vm));
xyInterpm = ppval(ppm, tInterpm);

% Show the result
figure(12)
plot(Vm,Im,'xr')
hold on

plot(xyInterpm(1,:),xyInterpm(2,:),'r')
hold on

plot(Vd,Id,'og');
hold on
plot(xxd,yyd,'g');
title('Comparation between points from the datasheet and the ones obtained in IV curve form')
xlabel('V (V)') 
ylabel('I (A)')
legend({'Resulted points','Interpolation','Datasheet points','Interpolation'})
ylim([0 15])

%% DC

PDClimit = 2.6e6+2.6e6*(1-0.986);

ModuleParametersJ.Voc0 = Voc0;

ModuleParametersJ.Vmp0 = Vmp0;

ModuleParametersJ.Imp0 = Imp0;
ModuleParametersJ

Ee(isnan(Ee))=0; % Set any NaNs to zero
mSAPMResults = pvl_sapm(ModuleParametersJ, Ee, ArrayTemp);
aSAPMResults.Vmp = Ns  *mSAPMResults.Vmp;                      
aSAPMResults.Imp = Np  *mSAPMResults.Imp;
aSAPMResults.Pmp = zeros(size(aSAPMResults.Vmp,1),1);
for i = 1:size(aSAPMResults.Vmp,1)
if (aSAPMResults.Vmp(i)*aSAPMResults.Imp(i))>PDClimit
    aSAPMResults.Imp(i) = PDClimit/aSAPMResults.Vmp(i);
    aSAPMResults.Pmp(i) = aSAPMResults.Vmp(i) .* aSAPMResults.Imp(i);
else 
    aSAPMResults.Imp(i) = Np  *mSAPMResults.Imp(i);
    aSAPMResults.Pmp(i) = aSAPMResults.Vmp(i) .* aSAPMResults.Imp(i);
end
end
% aSAPMResults.Pmp = aSAPMResults.Vmp .* aSAPMResults.Imp;

% Apply a percentage of 1.2 % of wiring loss fraction in DC wiring
WLF = 0.012;
RarraySTC = aSAPMResults.Vmp./aSAPMResults.Imp;
Rw = WLF./RarraySTC;
Ploss = 0.01;%Rw.*aSAPMResults.Imp./aSAPMResults.Vmp;
PlossPer = (Rw.*aSAPMResults.Imp.^2)./aSAPMResults.Pmp;
%aSAPMResults.Pmp = aSAPMResults.Pmp - aSAPMResults.Pmp*0.012;
aSAPMResults.Pmp = aSAPMResults.Pmp - aSAPMResults.Pmp.*Ploss;
% aSAPMResults.Pmp = aSAPMResults.Pmp - Ploss;

figure('OuterPosition',[100 100 600 800])
tiledlayout(3,1) % subplot(3,1,1)
% First plot
ax1 = nexttile;
plot(TimeAl1,aSAPMResults.Pmp/10^6,'-sr')
hold on
plot(TimeAlIVDC,I_DC.*V_DC/10^6,'-sg')
legend('Pmp','Real data DC power')
%xlabel('Seconds of the day')
ylabel('DC Power (MW)')
title('Al-Safawi, DC Power','FontSize',14)
% Second plot
ax2 = nexttile; % subplot(3,1,2)
plot(TimeAl1,aSAPMResults.Imp,'-ok')
hold on
plot(TimeAlIVDC,I_DC,'-sg')
legend('Imp','Real data DC current')
%xlabel('Seconds of the day')
ylabel('DC Current (A)')
title('Al-Safawi, Current','FontSize',12)
% Third plot
ax3 = nexttile; % subplot(3,1,3)
plot(TimeAl1,aSAPMResults.Vmp,'-xk')
hold on
plot(TimeAlIVDC,V_DC,'-sg')
legend('Vmp','Real data DC voltage')
xlabel('Minutes of the month') 
ylabel('DC Volatage (V)')
title('Al-Safawi, Voltage','FontSize',12)

linkaxes([ax1 ax2 ax3],'x')

%% DC to AC Conversion
  Nsp = Np; % Number of parallel strings
  Ni = 1;    % Number of inverters
%   PAC_InvMW = PAC_Inv11_1 * 10^3;
%   PAC_InvMW2 = PAC_Inv11_2 * 10^3;
  ACPowerp = pvl_snlinverter(SMAinverter, aSAPMResults.Vmp, aSAPMResults.Pmp);   %% Importante el numero de strings en paralelo por inversor
  % At 15ºC 2750 -> 1 and at 27 ºC 2625 -> 0.9545
  % Perdida de eficiencia por temeperatura sigue la recta y = -0.0038x + 1.0570
%   RedC = zeros(length(Inv1T),1);%zeros(length(AirtT_avg),1);
%   ACPower = zeros(length(ACPowerp),1);
%   for i=1:length(Inv1T)
%         if Inv2T(i)>15
%             RedC(i) = -0.0038*Inv1T(i)+1.0570;
%             ACPower(i) = ACPowerp(i)*RedC(i);  
%         else 
%             ACPower(i) = ACPowerp(i);
%         end
%   end
%   PF_Inv11_1 = PAC_Inv11_1./sqrt(PAC_Inv11_1.^2+QAC_Inv11_1.^2);
%   PF_Inv11_2 = PAC_Inv11_2./sqrt(PAC_Inv11_2.^2+QAC_Inv11_2.^2);
%   
%   ACPowerp1 = ACPower.*PF_Inv11_1;
%   ACPowerp2 = ACPower.*PF_Inv11_2;
%   ACPowerq1 = ACPower.*sin(acos(PF_Inv11_1));
%   ACPowerq2 = ACPower.*sin(acos(PF_Inv11_2));

% Apply a 2% of losses in AC wiring 
ACPowerp = ACPowerp - ACPowerp*0.01;

ACPower = ACPowerp;  
  %% active and reactive power AC
  figure(15)
  tiledlayout(2,1)
  % First plot
  ax1 = nexttile;
  plot(TimeAl1,aSAPMResults.Pmp/10^6,'-sr')
  hold on
  plot(TimeAl1,ACPower.*Ni/10^6,'-ob')   

  legend('DC Pmp', 'AC Power 1', 'Real DC Power injected 1', 'Real AC Power injected 1')
  xlabel('Minutes')
  ylabel('Power (MW)')
  title('Oruro AC Power Output ','FontSize',14)
  % Second plot
%   ax2 = nexttile;
%   plot(Time,ACPowerq1.*Ni/10^6,'-ob')
%   hold on
%   plot(Time,QAC_Inv11_1/10^3,'-sg')
%   linkaxes([ax1 ax2],'x')  
%   legend('Reactive AC Power 1', 'Reactive AC Power injected 1')
%   xlabel('Minutes')
%   ylabel('Power (MWAr)')
%   title('Oruro AC Reactive Power Output ','FontSize',14) 
  
%   figure(16)
%   tiledlayout(2,1)
%   % First plot
%   ax1 = nexttile;
%   plot(Time,aSAPMResults.Pmp/10^6,'-sr')
%   hold on
%   plot(Time,ACPowerp2.*Ni/10^6,'-ob')   
%   hold on
%   plot(Time,Pinj_Inv11_2/10^3,'-sy')
%   hold on
%   plot(Time,PAC_InvMW2/10^6,'-sg')
%   legend('DC Pmp', 'AC Power 2', 'Real DC Power injected 2', 'Real AC Power injected 2')
%   xlabel('Minutes')
%   ylabel('Power (MW)')
%   title('Oruro DC and AC Power Output ','FontSize',14)
%   % Second plot
%   ax2 = nexttile;
%   plot(Time,ACPowerq2.*Ni/10^6,'-ob')
%   hold on
%   plot(Time,QAC_Inv11_2/10^3,'-sg')
%   linkaxes([ax1 ax2],'x')  
%   legend('Reactive AC Power 2', 'Reactive AC Power injected 2')
%   xlabel('Minutes')
%   ylabel('Power (MWAr)')
%   title('Oruro DC and AC Reactive Power Output ','FontSize',14) 
%   
%   %% power factor
%   figure(17)
%   tiledlayout(3,1) % subplot(3,1,1)
% % First plot
% ax1 = nexttile;
% plot(Time,PFE_Inv11_1);
% hold on
% plot(Time,PF_Inv11_1);
% legend('PF excel', 'PF calculated')
%  xlabel('Time in minutes')
%  ylabel('cos(phi)')
%  title('Inverter 11_1 ','FontSize',14)
%  ylim([-1.5 1.5])
% % Second plot
% ax2 = nexttile; % subplot(3,1,2)
% plot(Time,PFE_Inv11_2);
% hold on
% plot(Time,PF_Inv11_2);
% legend('PF excel', 'PF calculated')
%  xlabel('Time in minutes')
%  ylabel('cos(phi)')
%  title('Inverter 11_2 ','FontSize',14)
%  ylim([-1.5 1.5])
% % Third plot
% ax3 = nexttile;
% plot(Time,[PF_Inv11_1,PF_Inv11_2])
% ylim([0 1.5])
% linkaxes([ax1 ax2 ax3],'x')

%% PERFOMANCE CALCULATION
%% PR
 Nipr = 22;
 ACPowerPRd = zeros(round(length(ACPower)/288),1);
 Irr1PRd = zeros(round(length(ACPower)/288),1);
 DCPowerPRd = zeros(round(length(ACPower)/288),1);
 j = 1;
 for i=1:length(ACPower)
    if mod(i,288)== 0
       ACPowerPRdmio(j) = sum(ACPower(i-287:i))/24*0.95;
       DCPowerPRd(j) = sum(I_DC(i-287:i).*V_DC(i-287:i))/24;
       Irr1PRd(j) = sum(Irr1(i-287:i))/24;
       j = j+1;
    end
 end
Yfmio = (ACPowerPRdmio.*Nipr)./(330*307*30*Nipr);
Yf = (DCPowerPRd.*Nipr)./(330*307*30*Nipr);
Yr = Irr1PRd/1000;
PR = Yf./Yr;
PRmio = Yfmio./Yr;
