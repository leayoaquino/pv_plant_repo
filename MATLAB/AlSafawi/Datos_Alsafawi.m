
TimeAl1 =  Estacinmeteo3{:,8};
TimeAl1 = datetime(TimeAl1,'InputFormat', 'dd/MM/yyyy HH:mm');
TimeAl2 =  Estacinmeteo7{:,8};
TimeAl2 = datetime(TimeAl2,'InputFormat', 'dd/MM/yyyy HH:mm');
TimeAl3 =  Estacinmeteo10{:,8};
TimeAl3 = datetime(TimeAl3,'InputFormat', 'dd/MM/yyyy HH:mm');

TimeAlIVDC = InversorcorrienteDCtensinDC{:,5};
TimeAlIVDC = datetime(TimeAlIVDC,'InputFormat', 'dd/MM/yyyy HH:mm');
TimeAlIVDCs = StringcorrienteDCtensinDC{:,5};
TimeAlIVDCs = datetime(TimeAlIVDCs,'InputFormat', 'dd/MM/yyyy HH:mm');
TimeAlIVDCsb = StringboxcorrienteDCtensinDC{:,5};
TimeAlIVDCsb = datetime(TimeAlIVDCsb,'InputFormat', 'dd/MM/yyyy HH:mm');


% Meteo
AirT_1 = Estacinmeteo3{:,3}; 
AirT_2 = Estacinmeteo7{:,3}; 
AirT_3 = Estacinmeteo10{:,3}; 
% 
SurfT_1 = Estacinmeteo3{:,4}; 
SurfT_2 = Estacinmeteo3{:,5}; 
SurfT_3 = Estacinmeteo7{:,4}; 
SurfT_4 = Estacinmeteo7{:,5};
SurfT_5 = Estacinmeteo10{:,4}; 
SurfT_6 = Estacinmeteo10{:,5};

Irr1 = Estacinmeteo3{:,6}; 
Irr2 = Estacinmeteo7{:,6}; 
Irr3 = Estacinmeteo10{:,6}; 

Windspeed1 = Estacinmeteo3{:,7}; 

% Tension y corriente en DC (Inversor 7.1)
I_DC = InversorcorrienteDCtensinDC{:,3};
V_DC = InversorcorrienteDCtensinDC{:,4};
% String
I_DCs = StringcorrienteDCtensinDC{:,3};
V_DCs = StringcorrienteDCtensinDC{:,4};
%string box
I_DCsb = StringboxcorrienteDCtensinDC{:,3};
V_DCsb = StringboxcorrienteDCtensinDC{:,4};

%% Cell temp calculation
a1 = -3.47;
b1 = -0.0594;
IncTcnd = 3;
a2 = -3.56;
b2 = -0.0750;
a3 = -3.58;
b3 = -0.1130;

    Tm1 = Irr1.*(exp(a1+b1*Windspeed1))+AirT_1;
    Tcell1 = Tm1 + (Irr1/1000)*IncTcnd;
    Tm2 = Irr1.*(exp(a2+b2*Windspeed1))+AirT_1;
    Tcell2 = Tm2 + (Irr1/1000)*IncTcnd;
    Tm3 = Irr1.*(exp(a3+b3*Windspeed1))+AirT_1;
    Tcell3 = Tm3 + (Irr1/1000)*IncTcnd;

figure(1)
plot(TimeAl1,SurfT_1)
hold on
plot(TimeAl1,Tcell1)
hold on
plot(TimeAl1,Tcell2)
hold on
plot(TimeAl1,Tcell3)
legend('Tsurface', 'Tcell Glass/cell/glass', 'Tcell Glass/cell/polymer sheet', 'Tcell Polymer/thin-film/steel')