%                                %&&&&&%                                          
%                               ,&&&&&&&&&&&&&&&&&&&%                             
%                               ,&&&&. %&&&&&&&&&&&&&&&(                          
%                                &&&&&.             &&&&&                         
%                                 &&&&&&.    ,,,,,  *&&&&&&&                      
%                                  &&&&&&(  .,,,,,     @&&&&&&                    
%                                 &&&&&..      ,      %&&&&&&&                    
%                               *&&&&&,           .&&&&&&&&.                      
%                       *#&&&&&&&&&&.          (&&&&&&*                           
%                   *&&&&&&&&&&&/             &&&&&&                              
%                &&&&&&&&*                  &&&&&&  %&&&&&&&&&&&.                 
%             &&&&&&&.                     /&&&&( &&&&&&&&&&&&&&&&&&&/            
%           &&&&&@                        .&&&&. &&&&/       &&&&&&&&&&&%         
%         &&&&&#                 #&&&&&&&%(&&&&  &&&&&#     &&&&&   ,&&&&&%       
%       /&&&&#                .&&&&&&&&&&&&&&&#    &&&&&&&&&&&&&       &&&&&&     
%      &&&&&,                #&&&&,      @&&&&#       *//@&&&&&&&&&  @&&&&&&&&    
%     &&&&&                 &&&&&         &&&&&                &&&&&&&&&& &&&&&   
%    &&&&&                 &&&&&          &&&&&                  &&&&&     &&&&%  
%   &&&&&                 %&&&&*         &&&&&&                   &&&&&    &&&&&  
%   &&&&/                &&&&&          &&&&&&&                   @&&&&&&&.&&&&&  
%  (&&&&                %&&&&          &&&&&&&&                  .&&&&&&&&&&&&&&  
%  &&&&&               &&&&&          &&&&&&&&&                 &&&&&/    &&&&&   
%  &&&&&              /&&&&*        .&&&&%&&&&&             (&&&&&&%     #&&&&*   
%  &&&&&              &&&&.         &&&&& &&&&&&&&&&&&&&&&&&&&&&&&&&&*  &&&&&     
%   &&&&(           *&&&&         *&&&&#  &&&&&&%@@@@@@&&&&&     .&&&&&&&&&       
%   &&&&&          *&&&&*        %&&&&(   (&&&&&&&&/   &&&&&      %&&&&&&         
%   .&&&&&        .&&&&          &&&&&         %&&&&#  &&&&&&&&&&&&&&&&           
%     &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
%               LEMUR RESEARCH GROUP. UNIVERSITY OF OVIEDO,SPAIN
%     &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

% PVEM is an emulator for Photovoltaic Systems. It was developed at the
% LEMUR Research Group (University of Oviedo). It is based in the PVLib
% Matlab implementation Copyright (c) 2015, Sandia Corporation
% Contact:blancocristian@uniovi.es


% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
% IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
% THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
% PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
% CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
% EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
% PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
% OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
% WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
% OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
% EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% % INPUTS

% Module: Struct containing the Module parameters. It is usually extracted
% from the CECModulesSAM.mat file
% Ns: Number of Series modules of a string
% Np: Number of strings per inverter
% POA: Point of array irradiance
% Windspeed_2m: Measured Windspeed at 2 meters
% CellTemp: Cell Temperature. It is usually obtained from the measured module
% temperature
% inverter:Struct containing the inverter parameters. It is usually
% extracted from the CECInverters.mat file. 
% method: 1: CEC with database
%         2: Sandia with calculation

% flag_MPP: 1: Knowing that the DC voltage is lower than the minimum value of the inverter, calculate the new current
%           0: The maximum power point is calculated

% topology: 1: Fixed
%           0: Tracker

% % OUTPUTS
% I_MPP:DC current (Maximum Point of Power)
% V_MPP:DC voltage (Maximum Point of Power)
% P_MPP:DC power (Maximum Point of Power)
% ACPower: AC power considering unity power factor

function [I_MPP,V_MPP,P_MPP,ACPower]=PVEM_0p1(Module,Ns,Np,POA,Windspeed_2m,CellTemp,inverter,method,flag_MPP,topology,tilt_deg)

% POA(isnan(POA))=0;
% CellTemp(isnan(CellTemp))=0;
POA=POA*(1-Module.LID);

if(topology)
    IAM=1-0.05*(1/cos((tilt_deg*pi/180))-1);
    POA=POA*(IAM);
end


%% DC parameter estimation

if(method==1)
    
%Aqui aplico directamente el modelo del single-diode con parámetros de la
%librería de CEC
[IL, I0, Rs, Rsh, a] = pvl_calcparams_CEC(POA,CellTemp,Module);

[IVResult] = pvl_singlediode(IL, I0, Rs, Rsh, a);

P_MPP=IVResult.Pmp*Ns*Np;
I_MPP= IVResult.Imp*Np;
V_MPP= IVResult.Vmp*Ns;
    

elseif (method==2)

S = [100 200 300 400 500 600 700 800 900 1000 1100 1200 1300]; % Irradiance Levels (W/m^2) for parameter sets
% To get the cell temperature we can use the equation Tcell = Tamb + 1/U · Ginc · Alpha · (1-effic)
Tcell = zeros(1,length(S));
Tm = zeros(1,length(S));
Tamb = 25;
U = 29;      % Heat transfer U-factor [W/m^2K]
Alpha = 0.9; % Absortion coeffcient of solar irradiation or identically 1 - Reflexion (typically 0.9)
effic = Module.effic;
for i=1:length(S)
    Tm(i) = Tamb + 1/U * S(i) * Alpha * (1-effic/100); 
    Tcell(i) = Tm(i) + (S(i)/1000)*3;
end

% a2 = -3.56;
% b2 = -0.0750;
% IncTcnd = 3;
% Tamb_calc=25*ones(1,length(S));
% Windspeed_2m_calc=ones(1,length(S));

% Tcell_PVL_calc = pvl_sapmcelltemp(S,1000,a2,b2, mean(Windspeed_2m)*ones(1,length(S)), Tamb*ones(1,length(S)), IncTcnd);

[IL, I0, Rs, Rsh, a] = pvl_calcparams_CEC(S,Tcell,Module);

NumPoints = 1000;
[IVResult] = pvl_singlediode(IL, I0, Rs, Rsh, a, NumPoints);


%% Transfer the obtained data 
Tm = Tcell';
IRR = S';
Isc = IVResult.Isc;
Imp = IVResult.Imp;
Voc = IVResult.Voc;
Vmp = IVResult.Vmp;
V = IVResult.V';
I = IVResult.I';

%% Irradiance and Temperature for a year
Ee = POA/1000;

ModuleParametersJ = PVCoc(Tm,IRR,Isc,Module.alpha_sc,Module.Isc0,Imp,Module.beta_oc,Voc,Vmp,V,I,Module.Ns);

ModuleParametersJ.Voc0 = Module.Voc0;
ModuleParametersJ.Vmp0 = Module.Vmp0;
ModuleParametersJ.Imp0 = Module.Imp0;

%Aplico el método de Sandia Labs para obtener los resultados en continua
mSAPMResults = pvl_sapm(ModuleParametersJ, Ee, CellTemp);

P_MPP=mSAPMResults.Pmp*Np*Ns;
I_MPP= Np*mSAPMResults.Imp;
V_MPP= Ns*mSAPMResults.Vmp;

end

if(flag_MPP)

    [Vmpp_corr,Impp_corr]=inverter_minvoltage(V_MPP,I_MPP,POA,CellTemp,Module,Ns,Np,800);
    P_MPP=Vmpp_corr.*Impp_corr;
    
    I_MPP= Impp_corr;
    V_MPP= Vmpp_corr;
end

% DC power losses, assume a 1%, it should be introduced by yourself
Ploss = 1.26/100;
ShadingLoss=0.86/100;%1.56
% IAM_Loss=2.56/100;

% Total_Loss=Ploss+ShadingLoss+IAM_Loss;
Total_Loss=ShadingLoss;

% Total_Loss=Ploss;

%% DC Limit

% PDClimit = 1.456e+06 + 1.456e+06 *(1-0.986);
% PDClimit = inf;

V_MPP(I_MPP>1)=V_MPP(I_MPP>1)-Ploss*P_MPP(I_MPP>1)./I_MPP(I_MPP>1);
% V_MPP(VMPP<0)=0;
P_MPP=V_MPP.*I_MPP*(1-Total_Loss);

%% DC to AC Conversion


  ACPower = pvl_snlinverter(inverter,V_MPP,P_MPP);   
  ACPower=ACPower';

  % Apply losses in AC wiring and Transformers
  AC_CableLosses=0.1/100;
  TransformerLosses=1.52/100;
  LossesAC=AC_CableLosses+TransformerLosses;

  ACPower = ACPower*(1-LossesAC);
  
end

