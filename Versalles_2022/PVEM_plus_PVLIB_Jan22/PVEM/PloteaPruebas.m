plot(TimeVer,aSAPMResults.Vmp)
hold on
plot(TimeVer,VDC_Inv1_2,'g')
legend('Vmp','Excel data DC voltage')
xlabel('Minutes') 
ylabel('DC Volatage (V)')
title('Versalles, Voltage','FontSize',12)


%% 

figure(2)
plot(TimeVer,aSAPMResults.Imp,'k')
hold on
plot(TimeVer,IDC_Inv1_2,'g')
legend('Imp','Excel data DC current')
%xlabel('Seconds of the day')
ylabel('DC Current (A)')
title('Versalles, Current','FontSize',12)



%% 
figure(3)
plot(TimeVer,aSAPMResults.Pmp/10^6)
hold on
plot(TimeVer,PDC_Inv1_2/10^3,'g')
legend('Pmp','Real data DC power')
%xlabel('Seconds of the day')
ylabel('DC Power (MW)')
title('Versalles, DC Power','FontSize',14)
