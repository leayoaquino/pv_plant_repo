%% REAL DATA
% Inversores
% Estaciones meteorológicas
TimeVer =  Inversores{:,1};

%% INVERSOR 1.2

PDC_Inv1_2 = Inversores{:,17};
VDC_Inv1_2 = Inversores{:,15};
IDC_Inv1_2 = Inversores{:,16};
PAC_Inv1_2 = Inversores{:,24};
QAC_Inv1_2 = Inversores{:,25};

%% Analizador de planta

Pplant = Analizadordeplanta{:,2};
Qplant = Analizadordeplanta{:,3};
PFplant = Analizadordeplanta{:,4}; 

%% Estación meteorologia

POA_1 = Estacionesmeteorolgicas{:,3};
POA_2 = Estacionesmeteorolgicas{:,4};
MODULET_1 = Estacionesmeteorolgicas{:,7};
MODULET_2 = Estacionesmeteorolgicas{:,8};
MODULET_3 = Estacionesmeteorolgicas{:,9};
Windspeed = Estacionesmeteorolgicas{:,5};
Tamb = Estacionesmeteorolgicas{:,6};


a_v=0.2; %Zona rústica = 0,2
Windspeed_10m=Windspeed/((4.5/10)^a_v);
Windspeed_2m = Windspeed_10m*(2/10)^a_v;

POA = (POA_1 + POA_2)/2;
MODULET = (MODULET_1 + MODULET_2 + MODULET_3)/3;
CELLT = MODULET + (POA/1000)*3;


%%
% NOCT data
effic     = 17.01                 ;
eta_c     = effic/100             ;
tau_alpha = 0.9                   ;
G_NOCT    = 800                   ; % W/m^2
% Tamb      = 25                    ;
T_a_NOCT  = 20                    ; % ºC
% v_w       = 1                     ; % m/s
T_NOCT    = 45.1                  ; % ºC
CELLT_NOCT = Tamb + (POA/G_NOCT) * (T_NOCT - T_a_NOCT) * (1 - eta_c / tau_alpha) .* (9.5 ./ (5.7 + 3.8 * Windspeed));

%% Cell temp calculation
a1 = -3.47;
b1 = -0.0594;
IncTcnd = 3;
a2 = -3.56;
b2 = -0.0750;
a3 = -3.58;
b3 = -0.1130;
Tm = POA.*(exp(a2+b2*Windspeed_2m)) + Tamb;
Tcell = Tm + (POA/1000)*IncTcnd;


a1 = -3.47;
b1 = -0.0594;
IncTcnd = 3;


Tcell_PVL = pvl_sapmcelltemp(POA, 1000, a1, b1, Windspeed_2m, Tamb, IncTcnd);

figure(1)
plot(TimeVer,CELLT)
hold on
plot(TimeVer,Tcell)
plot(TimeVer,CELLT_NOCT)
legend('Tcell', 'Tcell calculated','NOCT')


%% INJECTED POWER PLOT
% Minutes = 1:size(Pinj_Inv2);
% Time = datetime(Time,"InputFormat","dd/MM/yyyy HH:mm:ss");
% plot(Time,Pinj_Inv2*10^3) % In the excel the data is in KW
% hold on
% plot(Time,PDC_Inv2)