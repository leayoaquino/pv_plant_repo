
close all

angle=0:0.1:89;

IAM=1-0.05*(1./cos((angle*pi/180))-1);

iam_glass=physicaliam(angle./180*pi,1.526,4,0.002);
iam_plastic=physicaliam(angle./180*pi,1.256,4,0.002);

close all
figure(1)
plot(angle,IAM)
hold on
plot(angle,iam_glass)
plot(angle,iam_plastic)
grid on
ylim([0.95,1.02])
xlabel('angulo de incidencia (º)')
ylabel('Factor de pérdida')
legend('Ashrae','Fresnel Glass','Fresnel Plastic')
%% 




% plot(angle,IAM)
% hold on
plot(16,1-0.05*(1./cos((16*pi/180))-1),'ok')
plot(26,1-0.05*(1./cos((26*pi/180))-1),'ob')
plot(30,1-0.05*(1./cos((30*pi/180))-1),'or')
plot(36,1-0.05*(1./cos((36*pi/180))-1),'om')


grid on
ylim([0.95,1.02])
xlabel('angulo de incidencia (º)')
ylabel('Factor de pérdida')
