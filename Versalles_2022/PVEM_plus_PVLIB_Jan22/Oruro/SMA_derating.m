function Pac_dr=SMA_derating(Pac,Tamb)

% INVERTER DERATING ACCORDING TO THE CURVE PROVIDED BY SMA FOR ORURO  
  % At 15ºC 2750 -> 1 and at 26 ºC 2440
  % Efficiency loss due to temeperature follows:
Temp=[-24.97368421052631,15,26,35.065789473684205,59.868421052631575];
Power=[2750,2750,2639.705882352941,2397.0588235294117,0];

m_1=(Power(3)-Power(2))/(Temp(3)-Temp(2));
m_2=(Power(4)-Power(3))/(Temp(4)-Temp(3));
m_3=(Power(5)-Power(4))/(Temp(5)-Temp(4));
  
  for i=1:length(Pac)
        if Tamb(i)<=Temp(2)
            Pac(i) = Pac(i);
        elseif (Tamb(i)>Temp(2)) && (Tamb(i)<=Temp(3))
            P_1=Power(2)+(Tamb(i)-Temp(2))*m_1;
            Pac(i) = min(P_1*1e3,Pac(i));       
        elseif (Tamb(i)>Temp(3)) && (Tamb(i)<=Temp(4))
            P_1=Power(3)+(Tamb(i)-Temp(3))*m_2;
            Pac(i) = min(P_1*1e3,Pac(i)); 
        elseif (Tamb(i)>Temp(4)) && (Tamb(i)<=Temp(5))
            P_1=Power(4)+(Tamb(i)-Temp(4))*m_3;
            Pac(i) = min(P_1*1e3,Pac(i));           
        end
  end
  
  Pac_dr=Pac;