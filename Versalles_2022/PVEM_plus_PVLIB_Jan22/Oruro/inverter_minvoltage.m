function [Vmpp_corr,Impp_corr]=inverter_minvoltage(Vmp,Imp,POA,CELLT,Module,Ns,Np,inverter_Vmin)

inverter_Vmin=800;
Vmpp_corr=zeros(1,length(Vmp));
Impp_corr=Vmpp_corr;

[IL, I0, Rs, Rsh, a] = pvl_calcparams_CEC(POA,CELLT,Module);

for i=1:1:length(Vmp)
   
    if(Vmp(i)<inverter_Vmin) && (POA(i)>200)
       Vmpp_corr(i)=inverter_Vmin;
       Vmp_module=Vmpp_corr(i)/Ns;
       points=1000;
       [IVResult] = pvl_singlediode(IL(i), I0(i), Rs, Rsh(i), a(i),points);
       [minValue,closestIndex] = min(abs(IVResult.V-Vmp_module));
       Impp_corr(i)=IVResult.I(closestIndex)*Np;
    else
        Vmpp_corr(i)=Vmp(i);
        Impp_corr(i)=Imp(i);
        
    end
end






