clear;clc;close all;
load DatosCrudos.mat
% load Analizadordeplanta_onlyneededinputs.mat
% load Estacionesmeteorologicas_onlyneededinputs.mat

 
%% PVEM Call
ExtractData_Oruro;

PVModule_definition;
inverter_definition;

tilt_deg=16;%Fixed orientation. Tilt angle (deg)

Windspeed_2m=3*ones(1,length(Tamb)); %no dispongo de datos de viento para este ejemplo

% method=2;%Cueli
% [Imp,Vmp,Pmp,Pac]=PVEM_0p1(Module,Ns,Np,POA,Windspeed_2m,CELLT,inverter,method);


method=1;%CEC database - (Faster)
topology=1;%Fixed
voltage_flag=0; 

[Imp,Vmp,Pmp,Pac]=PVEM_0p1(Module,Ns,Np,POA,Windspeed_2m,CELLT,inverter,method,voltage_flag,topology,tilt_deg);

method=2;%Cueli
[Imp_CEC,Vmp_CEC,Pmp_CEC,Pac_CEC]=PVEM_0p1(Module,Ns,Np,POA,Windspeed_2m,CELLT,inverter,method,voltage_flag,topology,tilt_deg);
% voltage_flag=1; 
% [Imp_CEC,Vmp_CEC,Pmp_CEC,Pac_CEC]=PVEM_0p1(Module,Ns,Np,POA,Windspeed_2m,CELLT,inverter,method,voltage_flag);

% Pac_pre=Pac; %to test derating


%% SMA altitude derating

Pac=SMA_derating(Pac,Tamb);
Pac_CEC=SMA_derating(Pac_CEC,Tamb);
%% 

% figure
% % plot(TimeVer,(Vmp)./(VDC_Inv1_2))
% plot(Vmp);hold on;plot((VDC_Inv1_2))
% hold on
% 
% figure(2)
% % plot(TimeVer,(Imp)./(IDC_Inv1_2))
% plot(Imp);hold on;plot((IDC_Inv1_2))
% hold on

% figure(3)
% % plot(TimeVer,(Imp)./(IDC_Inv1_2))
% % plot(Pmp/1e3);hold on;plot((PDC_Inv1_2));plot((PDC_Inv1_1))
% plot(Pac/1e3);hold on;plot((PDC_Inv1_1))
% 
% % plot(Pac/1e3);hold on;plot((PDC_Inv1_1))
% % plot(Pac./1e3./PDC_Inv1_1)
% hold on
% legend('PVEM','meas')

% figure(1)
% % plot(TimeVer,(Imp)./(IDC_Inv1_2))
% % plot(Pmp/1e3);hold on;plot((PDC_Inv1_2));plot((PDC_Inv1_1))
% plot(Vmp);hold on;plot(Vmp_CEC)
% grid on
% hold on
% legend('CEC','Cueli','meas')


% figure(2)
% % plot(TimeVer,(Imp)./(IDC_Inv1_2))
% % plot(Pmp/1e3);hold on;plot((PDC_Inv1_2));plot((PDC_Inv1_1))
% plot(Imp);hold on;plot(Imp_CEC)
% grid on
% hold on
% legend('Correct','Orig')


figure(3)
% plot(TimeVer,(Imp)./(IDC_Inv1_2))
% plot(Pmp/1e3);hold on;plot((PDC_Inv1_2));plot((PDC_Inv1_1))
plot(Datetime,Pac_CEC/1e3);hold on;plot(Datetime,Pac/1e3);plot(Datetime,(PDC_Inv1_1))
grid on
hold on
ylim([-10,3000])
legend('CEC','Sandia','Real')
ylabel('kW')
% xlim([datetime(2019,10,01),datetime(2020,10,01)])
xlim([datetime(2020,01,07),datetime(2020,01,08)])


% figure(4)
% % plot(TimeVer,(Imp)./(IDC_Inv1_2))
% % plot(Pmp/1e3);hold on;plot((PDC_Inv1_2));plot((PDC_Inv1_1))
% plot(Datetime,Pac_CEC/1e3./PDC_Inv1_1);hold on;plot(Datetime,Pac/1e3./PDC_Inv1_1)
% grid on
% hold on
% ylim([0.9,1.1])
% legend('CEC','Sandia')
% ylabel('kW (AC)')
% % xlim([datetime(2019,10,01),datetime(2020,10,01)])
% % xlim([datetime(2020,07,31),datetime(2020,08,01)])
% xlim([datetime(2020,01,07),datetime(2020,01,08)])




% figure(3)
% % plot(TimeVer,(Imp)./(IDC_Inv1_2))
% % plot(Pmp/1e3);hold on;plot((PDC_Inv1_2));plot((PDC_Inv1_1))
% plot(Pac_CEC/1e3);hold on;plot(Pac/1e3);plot((PDC_Inv1_1))
% grid on
% hold on
% legend('CEC','Cueli','meas')


%% 
% PDC_Inv1_1(isnan(PDC_Inv1_1))=0;
% error_CEC=Pac_CEC(4.4e5:end)/1e3-PDC_Inv1_1(4.4e5:end);
% error_Cueli=Pac(4.4e5:end)/1e3-PDC_Inv1_1(4.4e5:end);
% 
% error_TOTAL_CEC=sum(abs(error_CEC))
% error_TOTAL_Cueli=sum(abs(error_Cueli))


% figure;
% subplot(211)
% plot(Pac/1e3);hold on;plot((PDC_Inv1_1))
% grid on
% subplot(212)
% plot(POA)
% grid on


% % % % % To check temperature Derating                
% % % figure;
% % % subplot(211)
% % % plot(Datetime,Pac/1e3);hold on;plot(Datetime,Pac_pre/1e3);plot(Datetime,(PDC_Inv1_1))
% % % grid on
% % % hold on
% % % ylim([-10,3000])
% % % legend('Derated','Original','Real')
% % % ylabel('kW')
% % % xlim([datetime(2020,03,05),datetime(2020,03,06)])
% % % grid on
% % % subplot(212)
% % % plot(Datetime,Tamb)
% % % grid on
% % % ylabel('ºC')
% % % xlim([datetime(2020,03,05),datetime(2020,03,06)])


% cd 'VersallesMATLAB'
