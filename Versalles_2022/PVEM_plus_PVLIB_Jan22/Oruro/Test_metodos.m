
%Aqui aplico directamente el modelo del single-diode con parámetros de la
%librería de CEC
POA(isnan(POA))=0;
CELLT(isnan(CELLT))=0;

LID=1.8/100; %PVSyst is 1.8%. Datasheet is up to 2.5%
POA=POA*(1-LID);
[IL, I0, Rs, Rsh, a] = pvl_calcparams_CEC(POA,CELLT,Module);


% %DESOTO
% EgRef = 1.121; %Reference band gap.
% C = -0.0002677;  %Band gap dependence on temperature.
% [IL_soto, I0_soto, Rs_soto, Rsh_soto, a_soto] = pvl_calcparams_desoto(POA, CELLT, Module.alpha_sc, Module, EgRef, C);

%PVSyst Model
% [IL_PVSyst, I0_PVSyst, Rs_PVSyst, Rsh_PVSyst, a_PVSyst] = pvl_calcparams_PVsyst(POA, CELLT, Module.alpha_sc, Module);

[IVResult] = pvl_singlediode(IL, I0, Rs, Rsh, a);
% [IVResult_soto] = pvl_singlediode(IL_soto, I0_soto, Rs_soto, Rsh_soto, a_soto);



Ploss = 1.26/100;
ShadingLoss=0.86/100;%1.56
IAM_Loss=2.56/100;

Total_Loss=Ploss+ShadingLoss+IAM_Loss;

P_MPP=IVResult.Pmp*(1-Total_Loss)*Ns*Np;
I_MPP= IVResult.Imp*Np;
V_MPP= IVResult.Vmp*Ns;

% 
% P_MPP_soto=IVResult_soto.Pmp*(1-Total_Loss)*Ns*Np;
% I_MPP_soto= IVResult_soto.Imp*Np;
% V_MPP_soto= IVResult_soto.Vmp*Ns;

  ACPower = pvl_snlinverter(inverter,V_MPP,P_MPP);  
%   ACPower_soto = pvl_snlinverter(inverter,V_MPP_soto,P_MPP_soto);   


  ACPower=ACPower';
%   ACPower_soto=ACPower_soto';

  % Apply losses in AC wiring and Transformers
  AC_CableLosses=0.1/100;
  TransformerLosses=1.52/100;
  LossesAC=AC_CableLosses+TransformerLosses;

  ACPower = ACPower*(1-LossesAC);
%   ACPower_soto= ACPower_soto*(1-LossesAC);
  
%   P_average=(Pac+ACPower+ACPower_soto)/3;
  P_average=(Pac+ACPower)/2;

  
  %% 
  
figure;
% subplot(211)
% plot(ACPower/1e3);hold on;plot((PDC_Inv1_1));plot(ACPower_soto/1e3);plot(Pac/1e3);plot((P_average)/1e3)
plot(ACPower/1e3);hold on;plot((PDC_Inv1_1));plot(Pac/1e3);plot((P_average)/1e3)
grid on
legend('CEC','Meas','Sandia','Average')

% legend('CEC','Meas','DeSoto','Average')
% subplot(212)
% plot(POA)
% grid on