%% PV MODULE
% First of all we need to know the module we are going to use or try

load('CECModulesSAM.mat')       % Huge datasheet with modules with CEC parameters


Name = ('JKMS330PP-72'); % Introduce el nombre del mÃ³dulo

% k = (strcmp({SandiaInverterDatabase.ModelNumber},Name)); % Encuentra el mÃ³dulo en el catÃ¡logo
% k=contains(SandiaInverterDatabase.ModelNumber,"2750-EV");
k=contains(CECModulesSAM.Name,Name);
index = find(k); % PosiciÃ³n del modulo dentro del catalogo

index=index(1);

% JKM330PP-72-V  # 10714
% Module = CECModuleSAM(10714);
% If we don't have the desired moudule in the datasheet we should introduce
% its characteristics
% Ind this case we have it, these values are the same as in Module = CECModuleSAM(10714);
Module.name = 'Jinko Solar JKM330PP-72';
Module.a_ref = CECModulesSAM(index,:).a_ref;%1.87478;
Module.IL_ref = CECModulesSAM(index,:).I_L_ref;%9.23276;
Module.I0_ref = CECModulesSAM(index,:).I_o_ref;%1.2592e-10;
Module.Rsh_ref = CECModulesSAM(index,:).R_sh_ref;%2754.66;
Module.Rs_ref = CECModulesSAM(index,:).R_s;%0.406344;
Module.alpha_sc = CECModulesSAM(index,:).alpha_sc;%0.005694;
Module.adjust = CECModulesSAM(index,:).Adjust;%9.24109;
% We need to include also the efficiency that appears on the module
% datasheet
Module.effic = 17.01; % in %
Module.Ns=CECModulesSAM(index,:).N_s;
Module.Isc0=CECModulesSAM(index,:).I_sc_ref;
Module.Voc0=CECModulesSAM(index,:).V_oc_ref;
Module.beta_oc=CECModulesSAM(index,:).beta_oc;

Module.Vmp0=CECModulesSAM(index,:).V_mp_ref;
Module.Imp0=CECModulesSAM(index,:).I_mp_ref;
Module.gamma_ref=CECModulesSAM(index,:).gamma_r;
Module.LID=1.8/100; %PVSyst is 1.8%. Datasheet is up to 2.5%



% ModuleParametersJ = PVCoc(Tm,IRR,Isc,Module.alpha_sc,Module.I_sc_ref,Imp,Module.beta_oc,Voc,Vmp,V,I,Module.N_s);


%% Define array configuration
Ns = 24;   % Number of modules in series
Np = 330; % Number of parallel strings



% ModuleParameters.gamma_ref=CECModulesSAM(index,:).gamma_r;
% ModuleParameters.mugamma=-0.052;
% % ModuleParameters.IL_ref=CECModuleSAM(10714).I_L_ref;
% % ModuleParameters.I0_ref=CECModuleSAM(10714).I_o_ref;
% % ModuleParameters.Rsh_ref=CECModuleSAM(10714).R_sh_ref;
% ModuleParameters.Rsh0=CECModuleSAM(10714).R_sh_ref;
% ModuleParameters.Rshexp=5.5;
% % ModuleParameters.Rs_ref=CECModuleSAM(10714).R_s;
% ModuleParameters.eG=0.99;
% ModuleParameters.Ns=CECModulesSAM(index,:).N_s;



% % % % JKM330PP-72-V  # 10714
% % % % Module = CECModuleSAM(10714);
% % % % If we don't have the desired moudule in the datasheet we should introduce
% % % % its characteristics
% % % % Ind this case we have it, these values are the same as in Module = CECModuleSAM(10714);
% % % Module.name = 'Jinko Solar JKM320PP-72';
% % % Module.a_ref = 1.87478;
% % % Module.IL_ref = 9.23276;
% % % Module.I0_ref = 1.2592e-10;
% % % Module.Rsh_ref = 2754.66;
% % % Module.Rs_ref = 0.406344;
% % % Module.alpha_sc = 0.005694;
% % % Module.adjust = 9.24109;
% % % % We need to include also the efficiency that appears on the module
% % % % datasheet
% % % Module.effic = 17.01; % in %
% % % 
% % % 
% % % 
% % % 
% % % % Tcell = [28 30 33 35 38 41 43 46 49 51 54 57 59]; %deg C;
% % % 
% % % 
% % % 
% % % % Notional cSi module
% % % ModuleParameters.gamma_ref=CECModuleSAM(10714).gamma_r;
% % % ModuleParameters.mugamma=-0.052;
% % % ModuleParameters.IL_ref=CECModuleSAM(10714).I_L_ref;
% % % ModuleParameters.I0_ref=CECModuleSAM(10714).I_o_ref;
% % % ModuleParameters.Rsh_ref=CECModuleSAM(10714).R_sh_ref;
% % % ModuleParameters.Rsh0=CECModuleSAM(10714).R_sh_ref;
% % % ModuleParameters.Rshexp=5.5;
% % % ModuleParameters.Rs_ref=CECModuleSAM(10714).R_s;
% % % ModuleParameters.eG=0.99;
% % % ModuleParameters.Ns=CECModuleSAM(10714).N_s;
% % % alpha_isc=CECModuleSAM(10714).alpha_sc;