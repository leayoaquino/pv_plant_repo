%% PV MODULE
% First of all we need to know the module we are going to use or try

%%Load modules datasheet
load('CECModulesSAM.mat');
%%Module name
Name = ('JKM330PP-72');
%%Find module name in catalogue
index = find(contains(CECModulesSAM.Name,Name)); 
%%Select the first module
index=index(1);

%%Load module information in "Module" structure
Module.name      = 'Jinko Solar JKM330PP-72'        ;
Module.a_ref     = CECModulesSAM(index,:).a_ref     ;
Module.IL_ref    = CECModulesSAM(index,:).I_L_ref   ;
Module.I0_ref    = CECModulesSAM(index,:).I_o_ref   ;
Module.Rsh_ref   = CECModulesSAM(index,:).R_sh_ref  ;
Module.Rs_ref    = CECModulesSAM(index,:).R_s       ;
Module.alpha_sc  = CECModulesSAM(index,:).alpha_sc  ;
Module.adjust    = CECModulesSAM(index,:).Adjust    ;
%%Module efficiency is extracted from datasheet
Module.effic     = 17.01                            ;
Module.Ns        = CECModulesSAM(index,:).N_s       ;
Module.Isc0      = CECModulesSAM(index,:).I_sc_ref  ;
Module.Voc0      = CECModulesSAM(index,:).V_oc_ref  ;
Module.beta_oc   = CECModulesSAM(index,:).beta_oc   ;
Module.Vmp0      = CECModulesSAM(index,:).V_mp_ref  ;
Module.Imp0      = CECModulesSAM(index,:).I_mp_ref  ;
Module.gamma_ref = CECModulesSAM(index,:).gamma_r   ;
%%PVSyst is 1.8%. Datasheet is up to 2.5%
Module.LID       = 2.5/100                          ;

%%Define array configuration
%%Panels per string
Ns = 30;
%%Number of Total parallel strings
Np_T = ( ( 10 + 25 + 20 + 15 + 10 ) + ( 30 + 25 + 20 + 15 + 9 ) ) * 4 * 30 / Ns ;
%%Number of parallel strings per inverter
Np = Np_T / 4;
% Np = 183;
