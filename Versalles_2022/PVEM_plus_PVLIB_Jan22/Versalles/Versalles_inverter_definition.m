%% Inverter
%%Load inverters database
load('CECInverters.mat')
%%The name of the module in the database. Voltage is different from
%%specifications, but it seems to have no impact on power calculation.
Name = ('INGETEAM POWER TECHNOLOGY S A : Ingecon Sun 1640TL U B630 Outdoor');
index = find(contains(CECInverters.Name,Name));

inverter.name        = 'INGECON SUN 1640TL B630'       ;
inverter.vac         = CECInverters(index,:).Vac       ;
inverter.Pac0        = CECInverters(index,:).Paco      ;
inverter.Pdc0        = CECInverters(index,:).Pdco      ;
inverter.Vdc0        = CECInverters(index,:).Vdco      ;
inverter.Ps0         = CECInverters(index,:).Pso       ;
inverter.C0          = CECInverters(index,:).C0        ;
inverter.C1          = CECInverters(index,:).C1        ;
inverter.C2          = CECInverters(index,:).C2        ;
inverter.C3          = CECInverters(index,:).C3        ;
inverter.Pnt         = CECInverters(index,:).Pnt       ;
inverter.Vdcmax      = CECInverters(index,:).Vdcmax    ;
inverter.Idcmax      = CECInverters(index,:).Idcmax    ;
inverter.MPPTLow     = CECInverters(index,:).Mppt_low  ;
inverter.MPPTHi      = CECInverters(index,:).Mppt_high ;
inverter.LibraryType = {'SandiaInverter'}              ;
inverter.LibraryName = {'Sandia Inverters'}            ;