%%%%%%Meteorological station - Data extraction

%%Datetime information
TimeVer                 = [datetime(ESTUDIO2020DICIEMBRE.Datetime)                   ];

%%Inverter 11
Inv11_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR11DCVoltageV)               ];
Inv11_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR11DCCurrentA)               ];
Inv11_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR11PowerFactor)              ];
Inv11_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR11ActivePowerkW)            ];
Data_Inv_11.VDC         = Inv11_DCVoltage_V                                           ;
Data_Inv_11.IDC         = Inv11_DCCurrent_A                                           ;
Data_Inv_11.pf          = Inv11_PowerFactor                                           ;
Data_Inv_11.PDC         = Inv11_ActivePower_kW                                        ;

%%Inverter 12
Inv12_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR12DCVoltageV)               ];
Inv12_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR12DCCurrentA)               ];
Inv12_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR12PowerFactor)              ];
Inv12_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR12ActivePowerkW)            ];
Data_Inv_12.VDC         = Inv12_DCVoltage_V                                           ;
Data_Inv_12.IDC         = Inv12_DCCurrent_A                                           ;
Data_Inv_12.pf          = Inv12_PowerFactor                                           ;
Data_Inv_12.PDC         = Inv12_ActivePower_kW                                        ;

%%Inverter 13
Inv13_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR13DCVoltageV)               ];
Inv13_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR13DCCurrentA)               ];
Inv13_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR13PowerFactor)              ];
Inv13_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR13ActivePowerkW)            ];
Data_Inv_13.VDC         = Inv13_DCVoltage_V                                           ;
Data_Inv_13.IDC         = Inv13_DCCurrent_A                                           ;
Data_Inv_13.pf          = Inv13_PowerFactor                                           ;
Data_Inv_13.PDC         = Inv13_ActivePower_kW                                        ;

%%Inverter 14
Inv14_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR14DCVoltageV)               ];
Inv14_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR14DCCurrentA)               ];
Inv14_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR14PowerFactor)              ];
Inv14_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR14ActivePowerkW)            ];
Data_Inv_14.VDC         = Inv14_DCVoltage_V                                           ;
Data_Inv_14.IDC         = Inv14_DCCurrent_A                                           ;
Data_Inv_14.pf          = Inv14_PowerFactor                                           ;
Data_Inv_14.PDC         = Inv14_ActivePower_kW                                        ;

%%Inverter 21
Inv21_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR21DCVoltageV)               ];
Inv21_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR21DCCurrentA)               ];
Inv21_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR21PowerFactor)              ];
Inv21_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR21ActivePowerkW)            ];
Data_Inv_21.VDC         = Inv21_DCVoltage_V                                           ;
Data_Inv_21.IDC         = Inv21_DCCurrent_A                                           ;
Data_Inv_21.pf          = Inv21_PowerFactor                                           ;
Data_Inv_21.PDC         = Inv21_ActivePower_kW                                        ;

%%Inverter 22
Inv22_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR22DCVoltageV)               ];
Inv22_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR22DCCurrentA)               ];
Inv22_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR22PowerFactor)              ];
Inv22_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR22ActivePowerkW)            ];
Data_Inv_22.VDC         = Inv22_DCVoltage_V                                           ;
Data_Inv_22.IDC         = Inv22_DCCurrent_A                                           ;
Data_Inv_22.pf          = Inv22_PowerFactor                                           ;
Data_Inv_22.PDC         = Inv22_ActivePower_kW                                        ;

%%Inverter 23
Inv23_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR23DCVoltageV)               ];
Inv23_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR23DCCurrentA)               ];
Inv23_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR23PowerFactor)              ];
Inv23_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR23ActivePowerkW)            ];
Data_Inv_23.VDC         = Inv23_DCVoltage_V                                           ;
Data_Inv_23.IDC         = Inv23_DCCurrent_A                                           ;
Data_Inv_23.pf          = Inv23_PowerFactor                                           ;
Data_Inv_23.PDC         = Inv23_ActivePower_kW                                        ;

%%Inverter 24
Inv24_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR24DCVoltageV)               ];
Inv24_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR24DCCurrentA)               ];
Inv24_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR24PowerFactor)              ];
Inv24_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR24ActivePowerkW)            ];
Data_Inv_24.VDC         = Inv24_DCVoltage_V                                           ;
Data_Inv_24.IDC         = Inv24_DCCurrent_A                                           ;
Data_Inv_24.pf          = Inv24_PowerFactor                                           ;
Data_Inv_24.PDC         = Inv24_ActivePower_kW                                        ;

%%Inverter 31
Inv31_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR31DCVoltageV)               ];
Inv31_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR31DCCurrentA)               ];
Inv31_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR31PowerFactor)              ];
Inv31_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR31ActivePowerkW)            ];
Data_Inv_31.VDC         = Inv31_DCVoltage_V                                           ;
Data_Inv_31.IDC         = Inv31_DCCurrent_A                                           ;
Data_Inv_31.pf          = Inv31_PowerFactor                                           ;
Data_Inv_31.PDC         = Inv31_ActivePower_kW                                        ;

%%Inverter 32
Inv32_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR32DCVoltageV)               ];
Inv32_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR32DCCurrentA)               ];
Inv32_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR32PowerFactor)              ];
Inv32_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR32ActivePowerkW)            ];
Data_Inv_32.VDC         = Inv32_DCVoltage_V                                           ;
Data_Inv_32.IDC         = Inv32_DCCurrent_A                                           ;
Data_Inv_32.pf          = Inv32_PowerFactor                                           ;
Data_Inv_32.PDC         = Inv32_ActivePower_kW                                        ;

%%Inverter 33
Inv33_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR33DCVoltageV)               ];
Inv33_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR33DCCurrentA)               ];
Inv33_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR33PowerFactor)              ];
Inv33_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR33ActivePowerkW)            ];
Data_Inv_33.VDC         = Inv33_DCVoltage_V                                           ;
Data_Inv_33.IDC         = Inv33_DCCurrent_A                                           ;
Data_Inv_33.pf          = Inv33_PowerFactor                                           ;
Data_Inv_33.PDC         = Inv33_ActivePower_kW                                        ;

%%Inverter 34
Inv34_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR34DCVoltageV)               ];
Inv34_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR34DCCurrentA)               ];
Inv34_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR34PowerFactor)              ];
Inv34_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR34ActivePowerkW)            ];
Data_Inv_34.VDC         = Inv34_DCVoltage_V                                           ;
Data_Inv_34.IDC         = Inv34_DCCurrent_A                                           ;
Data_Inv_34.pf          = Inv34_PowerFactor                                           ;
Data_Inv_34.PDC         = Inv34_ActivePower_kW                                        ;

%%Inverter 41
Inv41_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR41DCVoltageV)               ];
Inv41_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR41DCCurrentA)               ];
Inv41_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR41PowerFactor)              ];
Inv41_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR41ActivePowerkW)            ];
Data_Inv_41.VDC         = Inv41_DCVoltage_V                                           ;
Data_Inv_41.IDC         = Inv41_DCCurrent_A                                           ;
Data_Inv_41.pf          = Inv41_PowerFactor                                           ;
Data_Inv_41.PDC         = Inv41_ActivePower_kW                                        ;

%%Inverter 42
Inv42_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR42DCVoltageV)               ];
Inv42_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR42DCCurrentA)               ];
Inv42_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR42PowerFactor)              ];
Inv42_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR42ActivePowerkW)            ];
Data_Inv_42.VDC         = Inv42_DCVoltage_V                                           ;
Data_Inv_42.IDC         = Inv42_DCCurrent_A                                           ;
Data_Inv_42.pf          = Inv42_PowerFactor                                           ;
Data_Inv_42.PDC         = Inv42_ActivePower_kW                                        ;

%%Inverter 43
Inv43_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR43DCVoltageV)               ];
Inv43_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR43DCCurrentA)               ];
Inv43_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR43PowerFactor)              ];
Inv43_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR43ActivePowerkW)            ];
Data_Inv_43.VDC         = Inv43_DCVoltage_V                                           ;
Data_Inv_43.IDC         = Inv43_DCCurrent_A                                           ;
Data_Inv_43.pf          = Inv43_PowerFactor                                           ;
Data_Inv_43.PDC         = Inv43_ActivePower_kW                                        ;

%%Inverter 44
Inv44_DCVoltage_V       = [(ESTUDIO2020DICIEMBRE.INVERSOR44DCVoltageV)               ];
Inv44_DCCurrent_A       = [(ESTUDIO2020DICIEMBRE.INVERSOR44DCCurrentA)               ];
Inv44_PowerFactor       = [(ESTUDIO2020DICIEMBRE.INVERSOR44PowerFactor)              ];
Inv44_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.INVERSOR44ActivePowerkW)            ];
Data_Inv_44.VDC         = Inv44_DCVoltage_V                                           ;
Data_Inv_44.IDC         = Inv44_DCCurrent_A                                           ;
Data_Inv_44.pf          = Inv44_PowerFactor                                           ;
Data_Inv_44.PDC         = Inv44_ActivePower_kW                                        ;

%%Irradiance
Met_HorPyranometer_Wm2  = [(ESTUDIO2020DICIEMBRE.METEO01HorizontalPyranometer1Wm2)   ];
Met_IncPyranometer1_Wm2 = [(ESTUDIO2020DICIEMBRE.METEO01InclinedPyranometer1Wm2)     ];
Met_IncPyranometer2_Wm2 = [(ESTUDIO2020DICIEMBRE.METEO01InclinedPyranometer2Wm2)     ];

%%Temperature
Met_ExtAmbTemp_C        = [(ESTUDIO2020DICIEMBRE.METEO01ExternalAmbientTemperature1C)];
Met_ModuleTemp1_C       = [(ESTUDIO2020DICIEMBRE.METEO01ModuleTemperature1C)         ];
Met_ModuleTemp2_C       = [(ESTUDIO2020DICIEMBRE.METEO01ModuleTemperature2C)         ];
Met_ModuleTemp3_C       = [(ESTUDIO2020DICIEMBRE.METEO01ModuleTemperature3C)         ];

%%Plant Active Power
Plant_ActivePower_kW    = [(ESTUDIO2020DICIEMBRE.PlantActivePowerkW)                 ];


%%%%%%Meteorological station - Data calculation

%%Ambient temperature
Tamb = Met_ExtAmbTemp_C;

%%Irradiance - Should we include horizontal irradiance?
POA_1 = Met_IncPyranometer1_Wm2;
POA_2 = Met_IncPyranometer2_Wm2;
POA   = (POA_1 + POA_2) / 2    ; 

%%Temperature
MODULE_T1 = Met_ModuleTemp1_C                         ;
MODULE_T2 = Met_ModuleTemp2_C                         ;
MODULE_T3 = Met_ModuleTemp3_C                         ;
MODULET   = ( MODULE_T1 + MODULE_T2 + MODULE_T3 ) / 3 ;

%%Clean memory
clear('ESTUDIO*')
clear('Inv*')
clear('Met_*')
clear('MODULE_*')
clear('POA_*')

for i=1:1:length(MODULET)
    %%Saturate maximum temperature
    if(MODULET(i)>70)
        MODULET(i)=70;
    end
    %%Saturate minimum temperature
    if(MODULET(i)<-25)
        MODULET(i)=-25;
    end
    %%Delete NaN in irradiancia
    if(isnan(POA(i)))
        POA(i)=POA(i-1);
    end
    %%Delete NaN in module temperature
    if (isnan(MODULET(i)))
        MODULET(i)=MODULET(i-1);
    end
end


%%Cell temperature
CELLT = MODULET + (POA/1000)*3;