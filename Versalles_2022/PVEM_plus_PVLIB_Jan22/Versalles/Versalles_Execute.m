clear;clc;close all;

%%Load raw data
load VersallesDatosCrudos_Jul.mat

%%Process and extract reference data
Extract_Data_Versalles_Jul;

%%Load module data
VersallesPVModule_definition;

%%Load inverter data
Versalles_inverter_definition;

%%Tilt angle (deg). Fixed orientation
tilt_deg = 20;

%%Wind speed
Windspeed_2m=3*ones(1,length(Tamb));

%%Method 1: CEC with database - Method 2: Sandia with calculation
method       = 1;
topology     = 1;
voltage_flag = 0; 

if (method == 1)
    [Imp,Vmp,Pmp,Pac]                 = PVEM_0p1(Module,Ns,Np,POA,Windspeed_2m,CELLT,inverter,method,voltage_flag,topology,tilt_deg);
    
elseif (method == 2) 
    [Imp_CEC,Vmp_CEC,Pmp_CEC,Pac_CEC] = PVEM_0p1(Module,Ns,Np,POA,Windspeed_2m,CELLT,inverter,method,voltage_flag,topology,tilt_deg);
    
end

results_plot;

