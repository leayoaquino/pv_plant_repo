close all;

inverter_number      = '11'                               ;
inverter_struct_name = strcat('Data_Inv_',inverter_number);
eval(['inverter_struct = ',inverter_struct_name,';'])     ;

if (method == 1)
    
%     %%VDC normalized
%     figure();
%     plot(TimeVer,(Vmp)./(inverter_struct.VDC))
%     xlabel('Time');
%     ylabel('DC Voltage Ratio');
%     title(strcat('Inverter-',inverter_number,' - Estimated vs measured DC voltage ratio'));
    
    %%VDC comparison
    figure();
    plot(TimeVer,Vmp,'g');                  hold on;
    plot(TimeVer, inverter_struct.VDC,'b'); hold on;
    xlabel('Time');
    ylabel('DC Voltage [V]');
    title(strcat('Inverter-',inverter_number,' - Estimated vs measured DC voltage'));
    legend('Estimated','Measured')
    
%     %%IDC normalized
%     figure();
%     plot(TimeVer,(Imp)./(inverter_struct.IDC))
%     xlabel('Time');
%     ylabel('DC Current Ratio');
%     title(strcat('Inverter-',inverter_number,' - Estimated vs measured DC current ratio'));
    
    %%IDC comparison
    figure();
    plot(TimeVer,Imp,'g');                  hold on;
    plot(TimeVer, inverter_struct.IDC,'b'); hold on;
    xlabel('Time');
    ylabel('DC Current [A]');
    title(strcat('Inverter-',inverter_number,' - Estimated vs measured DC current'));
    legend('Estimated','Measured')
    
    %%PDC comparison
    figure();
    plot(TimeVer,Pmp/1e3,'g');                    hold on;
    plot(TimeVer, (inverter_struct.PDC)/1e3,'b'); hold on;
    xlabel('Time');
    ylabel('DC Power [kW]');
    title(strcat('Inverter-',inverter_number,' - Estimated vs measured DC Power'));
    legend('Estimated','Measured')
    
    %%PAC vs P comparison
    figure();
    plot(TimeVer,Pac/1e3,'g');              hold on;
    plot(TimeVer, inverter_struct.P,'b');   hold on;
    xlabel('Time');
    ylabel('Aparent Power [kVA] vs Active Power [kW]');
    title(strcat('Inverter-',inverter_number,' - Estimated Aparent Power [kVA] vs measured Active Power [kW]'));
    legend('Estimated','Measured')
end
