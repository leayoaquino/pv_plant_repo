%%%%%%Meteorological station - Data extraction


ESTUDIO2021ENERO(1,:) = [];
%%Datetime information
TimeVer                 = [datetime(ESTUDIO2021ENERO.Datetime)                   ];

%%Inverter 11
Inv11_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR11DCVoltageV)               ];
Inv11_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR11DCCurrentA)               ];
Inv11_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR11PowerFactor)              ];
Inv11_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR11ActivePowerkW)            ];
Inv11_DCPower_W         = Inv11_DCVoltage_V .* Inv11_DCCurrent_A                  ;
Data_Inv_11.VDC         = Inv11_DCVoltage_V                                       ;
Data_Inv_11.IDC         = Inv11_DCCurrent_A                                       ;
Data_Inv_11.pf          = Inv11_PowerFactor                                       ;
Data_Inv_11.PDC         = Inv11_DCPower_W                                         ;
Data_Inv_11.P           = Inv11_ActivePower_kW                                    ;

%%Inverter 12
Inv12_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR12DCVoltageV)               ];
Inv12_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR12DCCurrentA)               ];
Inv12_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR12PowerFactor)              ];
Inv12_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR12ActivePowerkW)            ];
Inv12_DCPower_W         = Inv12_DCVoltage_V .* Inv12_DCCurrent_A                  ;
Data_Inv_12.VDC         = Inv12_DCVoltage_V                                       ;
Data_Inv_12.IDC         = Inv12_DCCurrent_A                                       ;
Data_Inv_12.pf          = Inv12_PowerFactor                                       ;
Data_Inv_12.PDC         = Inv12_DCPower_W                                         ;
Data_Inv_12.P           = Inv12_ActivePower_kW                                    ;

%%Inverter 13
Inv13_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR13DCVoltageV)               ];
Inv13_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR13DCCurrentA)               ];
Inv13_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR13PowerFactor)              ];
Inv13_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR13ActivePowerkW)            ];
Inv13_DCPower_W         = Inv13_DCVoltage_V .* Inv13_DCCurrent_A                  ;
Data_Inv_13.VDC         = Inv13_DCVoltage_V                                       ;
Data_Inv_13.IDC         = Inv13_DCCurrent_A                                       ;
Data_Inv_13.pf          = Inv13_PowerFactor                                       ;
Data_Inv_13.PDC         = Inv13_DCPower_W                                         ;
Data_Inv_13.P           = Inv13_ActivePower_kW                                    ;

%%Inverter 14
Inv14_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR14DCVoltageV)               ];
Inv14_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR14DCCurrentA)               ];
Inv14_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR14PowerFactor)              ];
Inv14_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR14ActivePowerkW)            ];
Inv14_DCPower_W         = Inv14_DCVoltage_V .* Inv14_DCCurrent_A                  ;
Data_Inv_14.VDC         = Inv14_DCVoltage_V                                       ;
Data_Inv_14.IDC         = Inv14_DCCurrent_A                                       ;
Data_Inv_14.pf          = Inv14_PowerFactor                                       ;
Data_Inv_14.PDC         = Inv14_DCPower_W                                         ;
Data_Inv_14.P           = Inv14_ActivePower_kW                                    ;

%%Inverter 21
Inv21_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR21DCVoltageV)               ];
Inv21_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR21DCCurrentA)               ];
Inv21_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR21PowerFactor)              ];
Inv21_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR21ActivePowerkW)            ];
Inv21_DCPower_W         = Inv21_DCVoltage_V .* Inv21_DCCurrent_A                  ;
Data_Inv_21.VDC         = Inv21_DCVoltage_V                                       ;
Data_Inv_21.IDC         = Inv21_DCCurrent_A                                       ;
Data_Inv_21.pf          = Inv21_PowerFactor                                       ;
Data_Inv_21.PDC         = Inv21_DCPower_W                                         ;
Data_Inv_21.P           = Inv21_ActivePower_kW                                    ;

%%Inverter 22
Inv22_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR22DCVoltageV)               ];
Inv22_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR22DCCurrentA)               ];
Inv22_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR22PowerFactor)              ];
Inv22_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR22ActivePowerkW)            ];
Inv22_DCPower_W         = Inv22_DCVoltage_V .* Inv22_DCCurrent_A                  ;
Data_Inv_22.VDC         = Inv22_DCVoltage_V                                       ;
Data_Inv_22.IDC         = Inv22_DCCurrent_A                                       ;
Data_Inv_22.pf          = Inv22_PowerFactor                                       ;
Data_Inv_22.PDC         = Inv22_DCPower_W                                         ;
Data_Inv_22.P           = Inv22_ActivePower_kW                                    ;

%%Inverter 23
Inv23_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR23DCVoltageV)               ];
Inv23_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR23DCCurrentA)               ];
Inv23_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR23PowerFactor)              ];
Inv23_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR23ActivePowerkW)            ];
Inv23_DCPower_W         = Inv23_DCVoltage_V .* Inv23_DCCurrent_A                  ;
Data_Inv_23.VDC         = Inv23_DCVoltage_V                                       ;
Data_Inv_23.IDC         = Inv23_DCCurrent_A                                       ;
Data_Inv_23.pf          = Inv23_PowerFactor                                       ;
Data_Inv_23.PDC         = Inv23_DCPower_W                                         ;
Data_Inv_23.P           = Inv23_ActivePower_kW                                    ;

%%Inverter 24
Inv24_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR24DCVoltageV)               ];
Inv24_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR24DCCurrentA)               ];
Inv24_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR24PowerFactor)              ];
Inv24_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR24ActivePowerkW)            ];
Inv24_DCPower_W         = Inv24_DCVoltage_V .* Inv24_DCCurrent_A                  ;
Data_Inv_24.VDC         = Inv24_DCVoltage_V                                       ;
Data_Inv_24.IDC         = Inv24_DCCurrent_A                                       ;
Data_Inv_24.pf          = Inv24_PowerFactor                                       ;
Data_Inv_24.PDC         = Inv24_DCPower_W                                         ;
Data_Inv_24.P           = Inv24_ActivePower_kW                                    ;

%%Inverter 31
Inv31_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR31DCVoltageV)               ];
Inv31_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR31DCCurrentA)               ];
Inv31_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR31PowerFactor)              ];
Inv31_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR31ActivePowerkW)            ];
Inv31_DCPower_W         = Inv31_DCVoltage_V .* Inv31_DCCurrent_A                  ;
Data_Inv_31.VDC         = Inv31_DCVoltage_V                                       ;
Data_Inv_31.IDC         = Inv31_DCCurrent_A                                       ;
Data_Inv_31.pf          = Inv31_PowerFactor                                       ;
Data_Inv_31.PDC         = Inv31_DCPower_W                                         ;
Data_Inv_31.P           = Inv31_ActivePower_kW                                    ;

%%Inverter 32
Inv32_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR32DCVoltageV)               ];
Inv32_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR32DCCurrentA)               ];
Inv32_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR32PowerFactor)              ];
Inv32_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR32ActivePowerkW)            ];
Inv32_DCPower_W         = Inv32_DCVoltage_V .* Inv32_DCCurrent_A                  ;
Data_Inv_32.VDC         = Inv32_DCVoltage_V                                       ;
Data_Inv_32.IDC         = Inv32_DCCurrent_A                                       ;
Data_Inv_32.pf          = Inv32_PowerFactor                                       ;
Data_Inv_32.PDC         = Inv32_DCPower_W                                         ;
Data_Inv_32.P           = Inv32_ActivePower_kW                                    ;

%%Inverter 33
Inv33_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR33DCVoltageV)               ];
Inv33_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR33DCCurrentA)               ];
Inv33_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR33PowerFactor)              ];
Inv33_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR33ActivePowerkW)            ];
Inv33_DCPower_W         = Inv33_DCVoltage_V .* Inv33_DCCurrent_A                  ;
Data_Inv_33.VDC         = Inv33_DCVoltage_V                                       ;
Data_Inv_33.IDC         = Inv33_DCCurrent_A                                       ;
Data_Inv_33.pf          = Inv33_PowerFactor                                       ;
Data_Inv_33.PDC         = Inv33_DCPower_W                                         ;
Data_Inv_33.P           = Inv33_ActivePower_kW                                    ;

%%Inverter 34
Inv34_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR34DCVoltageV)               ];
Inv34_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR34DCCurrentA)               ];
Inv34_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR34PowerFactor)              ];
Inv34_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR34ActivePowerkW)            ];
Inv34_DCPower_W         = Inv34_DCVoltage_V .* Inv34_DCCurrent_A                  ;
Data_Inv_34.VDC         = Inv34_DCVoltage_V                                       ;
Data_Inv_34.IDC         = Inv34_DCCurrent_A                                       ;
Data_Inv_34.pf          = Inv34_PowerFactor                                       ;
Data_Inv_34.PDC         = Inv34_DCPower_W                                         ;
Data_Inv_34.P           = Inv34_ActivePower_kW                                    ;

%%Inverter 41
Inv41_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR41DCVoltageV)               ];
Inv41_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR41DCCurrentA)               ];
Inv41_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR41PowerFactor)              ];
Inv41_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR41ActivePowerkW)            ];
Inv41_DCPower_W         = Inv41_DCVoltage_V .* Inv41_DCCurrent_A                  ;
Data_Inv_41.VDC         = Inv41_DCVoltage_V                                       ;
Data_Inv_41.IDC         = Inv41_DCCurrent_A                                       ;
Data_Inv_41.pf          = Inv41_PowerFactor                                       ;
Data_Inv_41.PDC         = Inv41_DCPower_W                                         ;
Data_Inv_41.P           = Inv41_ActivePower_kW                                    ;

%%Inverter 42
Inv42_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR42DCVoltageV)               ];
Inv42_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR42DCCurrentA)               ];
Inv42_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR42PowerFactor)              ];
Inv42_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR42ActivePowerkW)            ];
Inv42_DCPower_W         = Inv42_DCVoltage_V .* Inv42_DCCurrent_A                  ;
Data_Inv_42.VDC         = Inv42_DCVoltage_V                                       ;
Data_Inv_42.IDC         = Inv42_DCCurrent_A                                       ;
Data_Inv_42.pf          = Inv42_PowerFactor                                       ;
Data_Inv_42.PDC         = Inv42_DCPower_W                                         ;
Data_Inv_42.P           = Inv42_ActivePower_kW                                    ;

%%Inverter 43
Inv43_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR43DCVoltageV)               ];
Inv43_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR43DCCurrentA)               ];
Inv43_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR43PowerFactor)              ];
Inv43_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR43ActivePowerkW)            ];
Inv43_DCPower_W         = Inv43_DCVoltage_V .* Inv43_DCCurrent_A                  ;
Data_Inv_43.VDC         = Inv43_DCVoltage_V                                       ;
Data_Inv_43.IDC         = Inv43_DCCurrent_A                                       ;
Data_Inv_43.pf          = Inv43_PowerFactor                                       ;
Data_Inv_43.PDC         = Inv43_DCPower_W                                         ;
Data_Inv_43.P           = Inv43_ActivePower_kW                                    ;

%%Inverter 44
Inv44_DCVoltage_V       = [(ESTUDIO2021ENERO.INVERSOR44DCVoltageV)               ];
Inv44_DCCurrent_A       = [(ESTUDIO2021ENERO.INVERSOR44DCCurrentA)               ];
Inv44_PowerFactor       = [(ESTUDIO2021ENERO.INVERSOR44PowerFactor)              ];
Inv44_ActivePower_kW    = [(ESTUDIO2021ENERO.INVERSOR44ActivePowerkW)            ];
Inv44_DCPower_W         = Inv44_DCVoltage_V .* Inv44_DCCurrent_A                  ;
Data_Inv_44.VDC         = Inv44_DCVoltage_V                                       ;
Data_Inv_44.IDC         = Inv44_DCCurrent_A                                       ;
Data_Inv_44.pf          = Inv44_PowerFactor                                       ;
Data_Inv_44.PDC         = Inv44_DCPower_W                                         ;
Data_Inv_44.P           = Inv44_ActivePower_kW                                    ;

%%Irradiance
Met_HorPyranometer_Wm2  = [(ESTUDIO2021ENERO.METEO01HorizontalPyranometer1Wm2)   ];
Met_IncPyranometer1_Wm2 = [(ESTUDIO2021ENERO.METEO01InclinedPyranometer1Wm2)     ];
Met_IncPyranometer2_Wm2 = [(ESTUDIO2021ENERO.METEO01InclinedPyranometer2Wm2)     ];

%%Temperature
Met_ExtAmbTemp_C        = [(ESTUDIO2021ENERO.METEO01ExternalAmbientTemperature1C)];
Met_ModuleTemp1_C       = [(ESTUDIO2021ENERO.METEO01ModuleTemperature1C)         ];
Met_ModuleTemp2_C       = [(ESTUDIO2021ENERO.METEO01ModuleTemperature2C)         ];
Met_ModuleTemp3_C       = [(ESTUDIO2021ENERO.METEO01ModuleTemperature3C)         ];

%%Plant Active Power
Plant_ActivePower_kW    = [(ESTUDIO2021ENERO.PlantActivePowerkW)                 ];


%%%%%%Meteorological station - Data calculation

%%Ambient temperature
Tamb = Met_ExtAmbTemp_C;

%%Irradiance - Should we include horizontal irradiance?
POA_1 = Met_IncPyranometer1_Wm2;
POA_2 = Met_IncPyranometer2_Wm2;
POA   = (POA_1 + POA_2) / 2    ; 

%%Temperature
MODULE_T1 = Met_ModuleTemp1_C                         ;
MODULE_T2 = Met_ModuleTemp2_C                         ;
MODULE_T3 = Met_ModuleTemp3_C                         ;
MODULET   = ( MODULE_T1 + MODULE_T2 + MODULE_T3 ) / 3 ;

%%Clean memory
clear('ESTUDIO*')
clear('Inv*')
clear('Met_*')
clear('MODULE_*')
clear('POA_*')

for i=1:1:length(MODULET)
    %%Saturate maximum temperature
    if(MODULET(i)>70)
        MODULET(i)=70;
    end
    %%Saturate minimum temperature
    if(MODULET(i)<-25)
        MODULET(i)=-25;
    end
    %%Delete NaN in irradiancia
    if(isnan(POA(i)))
        POA(i)=POA(i-1);
    end
    %%Delete NaN in module temperature
    if (isnan(MODULET(i)))
        MODULET(i)=MODULET(i-1);
    end
end


%%Cell temperature
CELLT = MODULET + (POA/1000)*3;